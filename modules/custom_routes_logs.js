var mysql = require('mysql');
var connection = require('./db');


module.exports.feed_syslogs = (
                                iduser = 0,
                                estatus = 'N/A',
                                mensaje = 'N/A',
                                to = 'N/A',
                                variables = 'N/A',
                                evento = 'N/A',
                                ip = 'N/A',
                                price = 'N/A',
                                priceunit = 'N/A',
                                from = 'N/A',
                                creado = 'N/A',
                                actualizado = 'N/A',
                                datasent = 'N/A',
                                direction = 'N/A',
                                errorcode = 'N/A',
                                errormessage = 'N/A',
                                messageservicesin = 'N/A',
                                nummedia = 'N/A',
                                numsegments = 'N/A',
                                sid  = 'N/A'
                               ) => {
                                let query = `INSERT INTO logs_notificaciones SET iduser = ?,
                                            estatus = ?,
                                            mensaje = ?,
                                            to_number = ?,
                                            variables = ?,
                                            evento = ?,
                                            ip = ?,
                                            price = ?,
                                            priceunit = ?,
                                            from_number = ?,
                                            creado = ?,
                                            actualizado = ?,
                                            datasent = ?,
                                            direction = ?,
                                            errorcode = ?,
                                            errormessage = ?,
                                            messageservicesin = ?,
                                            nummedia = ?,
                                            numsegments = ?,
                                            sid = ?`;
                                let inserts = [iduser,
                                                estatus,
                                                mensaje,
                                                to,
                                                variables,
                                                evento,
                                                ip,
                                                price,
                                                priceunit,
                                                from,
                                                creado,
                                                actualizado,
                                                datasent,
                                                direction,
                                                errorcode,
                                                errormessage,
                                                messageservicesin,
                                                nummedia,
                                                numsegments,
                                                sid];
                                query = mysql.format(query,inserts);
                                connection.getConnection(function (err, conn) {
                                    conn.query(query, function (error, results, field) {
                                        if (error){
                                            console.log("Error al guardar log: " + error);
                                            console.log(query)
                                        }else{
                                            console.log(`LOG: empleado ${iduser} | ip ${ip} | evento ${evento} | variables ${variables}`)
                                        }
                                    });
                                    conn.release();
                                });
                            }
