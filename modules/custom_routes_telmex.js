middleware = require("./middlewares");
var mysql = require("mysql");
var connection = require("./db");
var moment = require("moment");

module.exports = function (app, io) {
    app.post('/guardar_portabilidad', middleware.requireLogin, (req, res) => {
        let {asesor,
            nombres,
            paterno,
            materno,
            celular,
            fijo,
            email,
            horariovisitalaboral,
            numeroaportar,
            calle,
            lote,
            cruzamientos,
            colonia,
            codigopostal,
            ciudad,
            estado,
            entidadfederativa,
            estatuslinea,
            tipocliente,
            tiposervicio,
            tiposervicioresidencial} = req.body;
        
        if (nombres == "") nombres = null
        if (paterno == "")         paterno = null
        if (materno == "")         materno = null
        if (celular == "")         celular = null
        if (fijo == "")         fijo = null
        if (email == "")         email = null
        if (horariovisitalaboral == "")         horariovisitalaboral = null
        if (numeroaportar == "")         numeroaportar = null
        if (calle == "")         calle = null
        if (lote == "")         lote = null
        if (cruzamientos == "")         cruzamientos = null
        if (colonia == "")         colonia = null
        if (codigopostal == "")         codigopostal = null
        if (ciudad == "")         ciudad = null
        if (estado == "")         estado = null
        if (entidadfederativa == "")         entidadfederativa = null
        if (estatuslinea == "")         estatuslinea = null
        if (tipocliente == "")         tipocliente = null
        if (tiposervicio == "")         tiposervicio = null
        if (tiposervicioresidencial == "")         tiposervicioresidencial = null


        let query = `
        INSERT INTO exploratmk.telmex_portabilidad
        (
        asesor,
        nombres,
        paterno,
        materno,
        celular,
        fijo,
        email,
        horariovisitalaboral,
        numeroaportar,
        calle,
        lote,
        cruzamientos,
        colonia,
        codigopostal,
        ciudad,
        estado,
        entidadfederativa,
        estatuslinea,
        tipocliente,
        tiposervicio,
        tiposervicioresidencial)
        VALUES
        (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);
        
        `;
        let inserts = [asesor,
            nombres,
            paterno,
            materno,
            celular,
            fijo,
            email,
            horariovisitalaboral,
            numeroaportar,
            calle,
            lote,
            cruzamientos,
            colonia,
            codigopostal,
            ciudad,
            estado,
            entidadfederativa,
            estatuslinea,
            tipocliente,
            tiposervicio,
            tiposervicioresidencial];
        query = mysql.format(query,inserts);
        connection.getConnection(function (err, conn) {
            let connection_error = {
                status: 'error',
                mensaje: 'Error al conectar con la base de datos'
            }
            if (err) res.send(connection_error)
            conn.query(query, function (error, results, field) {
                if (error){
                    let response = {
                        status: 'error',
                        mensaje: error.sqlMessage
                    }
                    res.send(response)
                }else{
                    let response = {
                        status: 'success',
                        mensaje: results
                    }
                    res.send(response)
                }
            });
            conn.release();
        });

        
        
    })
    app.post('/guardar_activacion', middleware.requireLogin, (req, res) => {
        let {
            asesor,
            selectactivacion_tipodecaptura,
            activacion_nombres,
            activacion_paterno,
            activacion_materno,
            activacion_nombrecomercial,
            activacion_nombredefacturacion,
            activacion_rfc,
            selectactivacion_tipodeidentificacion,
            activacion_numerodeidentificacion,
            activacion_celular,
            activacion_telefonoadicional,
            activacion_correoelectronico,
            activacion_estado,
            activacion_municipio,
            activacion_colonia,
            activacion_cp,
            activacion_calle,
            activacion_numeroexterior,
            activacion_numerointerior,
            activacion_manzana,
            activacion_lote,
            activacion_edificio,
            activacion_departamento,
            activacion_calificador,
            activacion_subnumero,
            activacion_entrecalleuno,
            activacion_entrecalledos,
            selectactivacion_tiposervicio_,
            selectactivacion_paqueteinfinitum,
            selectactivacion_conentretenimiento,
            selectactivacion_cierre,
            activacion_siac
        } = req.body;

        if (asesor == '') asesor = null;
        if (selectactivacion_tipodecaptura == '') selectactivacion_tipodecaptura = null;
        if (activacion_nombres == '') activacion_nombres = null;
        if (activacion_paterno == '') activacion_paterno = null;
        if (activacion_materno == '') activacion_materno = null;
        if (activacion_nombrecomercial == '') activacion_nombrecomercial = null;
        if (activacion_nombredefacturacion == '') activacion_nombredefacturacion = null;
        if (activacion_rfc == '') activacion_rfc = null;
        if (selectactivacion_tipodeidentificacion == '') selectactivacion_tipodeidentificacion = null;
        if (activacion_numerodeidentificacion == '') activacion_numerodeidentificacion = null;
        if (activacion_celular == '') activacion_celular = null;
        if (activacion_telefonoadicional == '') activacion_telefonoadicional = null;
        if (activacion_correoelectronico == '') activacion_correoelectronico = null;
        if (activacion_estado == '') activacion_estado = null;
        if (activacion_municipio == '') activacion_municipio = null;
        if (activacion_colonia == '') activacion_colonia = null;
        if (activacion_cp == '') activacion_cp = null;
        if (activacion_calle == '') activacion_calle = null;
        if (activacion_numeroexterior == '') activacion_numeroexterior = null;
        if (activacion_numerointerior == '') activacion_numerointerior = null;
        if (activacion_manzana == '') activacion_manzana = null;
        if (activacion_lote == '') activacion_lote = null;
        if (activacion_edificio == '') activacion_edificio = null;
        if (activacion_departamento == '') activacion_departamento = null;
        if (activacion_calificador == '') activacion_calificador = null;
        if (activacion_subnumero == '') activacion_subnumero = null;
        if (activacion_entrecalleuno == '') activacion_entrecalleuno = null;
        if (activacion_entrecalledos == '') activacion_entrecalledos = null;
        if (selectactivacion_tiposervicio_ == '') selectactivacion_tiposervicio_ = null;
        if (selectactivacion_paqueteinfinitum == '') selectactivacion_paqueteinfinitum = null;
        if (selectactivacion_conentretenimiento == '') selectactivacion_conentretenimiento = null;
        if (selectactivacion_cierre == '') selectactivacion_cierre = null;
        if (activacion_siac == '') activacion_siac = null;


        let query = `
        INSERT INTO exploratmk.telmex_activaciones(
        asesor,
        tipodecaptura,
        nombres,
        paterno,
        materno,
        nombrecomercial,
        nombrefacturacion,
        rfc,
        tipodeidentificacion,
        numerodeidentificacion,
        celular,
        telefonoadicional,
        correoelectronico,
        estado,
        municipio,
        colonia,
        cp,
        calle,
        numeroexterior,
        numerointerior,
        manzana,
        lote,
        edificio,
        departamento,
        calificador,
        subnumero,
        entrecalleuno,
        entrecalledos,
        tiposervicio,
        paqueteinfinitum,
        conentretenimiento,
        cierre,
        siac)
        VALUES
        (?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?,
        ?)
        `;
        let inserts = [asesor,
            selectactivacion_tipodecaptura,
            activacion_nombres,
            activacion_paterno,
            activacion_materno,
            activacion_nombrecomercial,
            activacion_nombredefacturacion,
            activacion_rfc,
            selectactivacion_tipodeidentificacion,
            activacion_numerodeidentificacion,
            activacion_celular,
            activacion_telefonoadicional,
            activacion_correoelectronico,
            activacion_estado,
            activacion_municipio,
            activacion_colonia,
            activacion_cp,
            activacion_calle,
            activacion_numeroexterior,
            activacion_numerointerior,
            activacion_manzana,
            activacion_lote,
            activacion_edificio,
            activacion_departamento,
            activacion_calificador,
            activacion_subnumero,
            activacion_entrecalleuno,
            activacion_entrecalledos,
            selectactivacion_tiposervicio_,
            selectactivacion_paqueteinfinitum,
            selectactivacion_conentretenimiento,
            selectactivacion_cierre,
            activacion_siac];
        query = mysql.format(query,inserts);
        connection.getConnection(function (err, conn) {
            let connection_error = {
                status: 'error',
                mensaje: 'Error al conectar con la base de datos'
            }
            if (err) res.send(connection_error)
            conn.query(query, function (error, results, field) {
                if (error){
                    let response = {
                        status: 'error',
                        mensaje: error.sqlMessage
                    }
                    res.send(response)
                }else{
                    let response = {
                        status: 'success',
                        mensaje: results
                    }
                    res.send(response)
                }
            });
            conn.release();
        });
    })
}