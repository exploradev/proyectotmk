middleware = require('./middlewares');
var mysql = require('mysql');
var connection = require('./db');
var moment = require('moment');
let logs = require('./custom_routes_logs')

const accountSid = 'AC713bb0fecdb0ff5752a88dde39fcb2af'
const authToken = 'e1edb4b14ec760044a838cb1e9dc0cd6'
const client = require('twilio')(accountSid, authToken); 

// client.messages.create({
//                  from: '+13017109139',
//                  body: 'Ejemplo de mensaje twilio con env var',
//                  to: contacto
//                })
//                 .then(message => console.log(message));

module.exports = function(app,io){

    //LLENA LA TABLA DEL PERFIL DE ANALISTA DE ACUERDO AL STATUS SOLICITADO
    //DEVUELVE LOS DATOS DE ACUERDO A SI EL STATUS SE DEPURA MENSUALMENTE
    //O SI ES PERSISTENTE PARA SU DEBIDO SEGUIMIENTO
    app.post('/getDataForTableAnalista', middleware.requireLogin, function (req, res) {
        var status = req.body.status;

        var statusPorDepurarAlMes = ['Cambio a sinergia','Cancelada por cliente', 'Cancelada/Err.Asesor','Cancelada/Err.Mensajeria','Guia entregada'];

        if(status == 'todos'){
            var data = "select m.created , concat(u.name,' ',u.lastname) as asesor ,s.clientname,s.contact, m.guidenumber,  m.status, m.comment, m.idshipping from metashipping m left join shipping s on m.idshipping = s.idshipping left join user u ON m.iduser = u.iduser where (status not in ('Cambio a sinergia', 'Cancelada por cliente', 'Cancelada/Err.Asesor', 'Cancelada/Err.Mensajeria', 'Guia entregada')) or (status in ('Cambio a sinergia', 'Cancelada por cliente', 'Cancelada/Err.Asesor', 'Cancelada/Err.Mensajeria', 'Guia entregada') AND month(created) = month(now()))";
        } else if (statusPorDepurarAlMes.indexOf(status) == -1){
            var data = 'select m.created , concat(u.name," ",u.lastname) as asesor ,s.clientname,s.contact, m.guidenumber,  m.status, m.comment, m.idshipping from metashipping m left join shipping s on m.idshipping = s.idshipping left join user u ON m.iduser = u.iduser where status = ? ';
        }else{
            var data = 'select m.created , concat(u.name," ",u.lastname) as asesor ,s.clientname,s.contact, m.guidenumber,  m.status, m.comment, m.idshipping from metashipping m left join shipping s on m.idshipping = s.idshipping left join user u ON m.iduser = u.iduser where status = ? AND month(created) = month(now())';
        }

        var response_previsualizacion = []
        var inserts = [status];
        var query = mysql.format(data, inserts);
        connection.getConnection(function (err, conn) {
            conn.query(query, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {

                        'created': moment(element.created).format('D[/]MM[/]YY, H:mm'),
                        'asesor': element.asesor,
                        'cliente': element.clientname,
                        'contacto': element.contact,
                        'guidenumber': element.guidenumber,
                        'status': element.status,
                        'comments': element.comment,
                        'idshipping': element.idshipping,

                    }
                    response_previsualizacion.push(row);
                });
                res.send(response_previsualizacion);
            });
            conn.release();
        });
        
    });

    //DEVUELVE EL CONTEO POR STATUS PARA LLENAR LOS "BOTONES FILTRO" DEL PERFIL
    //DEL ANALISTA
    app.get('/getDataLabelsCount', middleware.requireLogin, function (req, res) {
        var response_counter = [];
        var query = "select SUM(CASE WHEN status = 'Guia entregada'  AND month(created) = month(now()) THEN 1 ELSE 0 END) AS 'Guia entregada', SUM(CASE WHEN status = 'Cancelada por cliente' AND month(created) = month(now()) THEN 1 ELSE 0 END) AS 'Cancelada por cliente', SUM(CASE WHEN status = 'Cambio a sinergia'  AND month(created) = month(now()) THEN 1 ELSE 0 END) AS 'Cambio a sinergia', SUM(CASE WHEN status = 'Cancelada/Err.Asesor'  AND month(created) = month(now()) THEN 1 ELSE 0 END) AS 'Cancelada/Err.Asesor', SUM(CASE WHEN status = 'Guia enviada'  THEN 1 ELSE 0 END) AS 'Guia enviada', SUM(CASE WHEN status = 'Err.Asesor/No tiene ID'  THEN 1 ELSE 0 END) AS 'Err.Asesor/No tiene ID', SUM(CASE WHEN status = 'Cliente no contesta'   THEN 1 ELSE 0 END) AS 'Cliente no contesta', SUM(CASE WHEN status = 'Trámite incompleto'   THEN 1 ELSE 0 END) AS 'Trámite incompleto', SUM(CASE WHEN status = 'Err.Asesor/Nombre erroneo'  THEN 1 ELSE 0 END) AS 'Err.Asesor/Nombre erroneo', SUM(CASE WHEN status = 'Equipo no disponible'  THEN 1 ELSE 0 END) AS 'Equipo no disponible', SUM(CASE WHEN status = 'No cuenta con pago'  THEN 1 ELSE 0 END) AS 'No cuenta con pago', SUM(CASE WHEN status = 'Confirmado/Pend.Envío'   THEN 1 ELSE 0 END) AS 'Confirmado/Pend.Envío', SUM(CASE WHEN status = 'pendiente'  THEN 1 ELSE 0 END) AS 'pendiente', SUM(CASE WHEN status = 'Cancelada/Err.Mensajeria' AND month(created) = month(now()) THEN 1 ELSE 0 END) AS 'Cancelada/Err.Mensajeria' from metashipping";
        
        connection.getConnection(function (err, conn) {
            conn.query(query, function (error, results, field) {
                if (error) throw error;
                
                res.send(results);
            });
            conn.release();
        });
        
    });

    //--------------------------------------------------------
    //LA RUTA PARA LLENAR EL MODAL PRINCIPAL DEL ANALISTA ESTA EN:
    //custom_routes_asesor.js
    //nombrado '/getDetalleDomicilio' 
    //RECIBE COMO PARAMETRO EL 'idshipping' QUE CORRESPONDE
    //AL ID DE LA CAPTURA A DOMICILIO REALIZADA
    //-------------------------------------------------------

    app.post('/updatedatadomicilio', middleware.requireLogin, function (req, res) {

        //se reciben los parametros del lado del cliente

        //idshipping
        var idshipping = req.body.idshipping;

        //var userLoggedIn = req.body.userLoggedIn;
        var cliente = req.body.cliente;
        var fechaentrega = req.body.fechaentrega;
        var horarioentrega = req.body.horarioentrega;
        var contacto = req.body.contacto;
        var numamigrar = req.body.numamigrar;
        var destino = req.body.destino;
        var direccion = req.body.direccion;
        var colonia = req.body.colonia;
        var referencias = req.body.referencias;
        var enganche = req.body.enganche;
        var folio = req.body.folio;
        var canal = req.body.canal;
        var plan = req.body.plan;
        var tipoactivacion = req.body.tipoactivacion;
        var ctaconsolidar = req.body.ctaconsolidar;
        var email = req.body.email;
        var financiamiento = req.body.financiamiento;
        var equipo = req.body.equipo;
        var imei = req.body.imei;
        var concepto = req.body.concepto;
        var observaciones = req.body.observaciones;
        var statusofrequest = req.body.statusofrequest;

        var factura = req.body.factura;
        var iccid = req.body.iccid;
        var guia = req.body.guia;
        var proveedor = req.body.proveedor;
        var activacion = req.body.activacion;
        var envio = req.body.envio;
        var entregado = req.body.entregado;
        var observacionanalista =  req.body.observacionanalista;

        //campo agregado
        var formapago = req.body.formapago;

        if (fechaentrega == '') { fechaentrega = null }
        if (envio == '') { envio = null }
        if (activacion == '') { activacion = null }
        if (entregado == '') { entregado = null }

         
        
        connection.getConnection(function (err, pool) {
            pool.beginTransaction(function (err) {
                if (err) throw err;
                var query_metashipping = "UPDATE metashipping SET paymethod = ?, status = ?, updatedstatus = now(), provider = ?, guidenumber = ?, comment = ?, datewithprovider = ?, iccid = ?, invoice = ?, activation_date = ?, delivered_date = ?, enganche = ? WHERE idshipping = ?";
                var inserts_metashipping = [formapago,statusofrequest,proveedor,guia,observacionanalista,envio,iccid,factura,activacion,entregado,enganche,idshipping];
                var query_insert_metashipping = mysql.format(query_metashipping, inserts_metashipping);
                
                pool.query(query_insert_metashipping, function (err, result) {
                    if (err) {
                        pool.rollback(function () {
                            throw err;
                        });
                    }

                    //siguiente query

                    var query_datashipping = "UPDATE datashipping SET sisact = ?,channel = ?,idplan = ?,type = ?,account = ?,email = ?,fineq = ?,phone = ?,imei = ?,concept = ?,additionalcomment = ? WHERE idshipping = ?";
                    var inserts_datashipping = [folio, canal, plan, tipoactivacion, ctaconsolidar, email, financiamiento, equipo, imei, concepto, observaciones, idshipping];
                    var query_insert_datashipping = mysql.format(query_datashipping, inserts_datashipping);

                    pool.query(query_insert_datashipping, function (err, result) {
                        if (err) {
                            pool.rollback(function () {
                                throw err;
                            });//fin del throw err
                        }//fin del if

                        var query_shipping = "UPDATE shipping SET clientname = ?,scheduled = ?,time = ?,contact = ?,mainline = ?,city = ?,address = ?,postalcode = ?,reference = ? WHERE idshipping = ?";
                        var inserts_shipping = [cliente, fechaentrega, horarioentrega, contacto, numamigrar, destino, direccion, colonia, referencias, idshipping];
                        var query_insert_shipping = mysql.format(query_shipping, inserts_shipping);

                        pool.query(query_insert_shipping, function (err, result) {
                            if (err) {
                                pool.rollback(function () {
                                    throw err;
                                });//fin del throw err
                            }//fin del if


                            //en el ultimo insert
                            pool.commit(function (err) {
                                if (err) {
                                    pool.rollback(function () {
                                        throw err;
                                    });
                                }
                                console.log('Actualizada sol. DOMICILIO ID: ' + idshipping);
                                res.send("Correcto");

                                

                                if(equipo == ""){
                                    var mensajeWhats = `Se ha generado con éxito la guía ${guia} para la entrega de su línea nueva con folio ${folio}.\nFavor de tener copia legible de la identificación y original de la misma para la entrega.\n\nLa guía la puede monitorear en https://alfmensajeria.com.mx`;
                                }else{
                                    var mensajeWhats = `Se ha generado con éxito la guía ${guia} para la entrega de su linea nueva con folio ${folio}.\nEquipo: ${equipo}\nPrecio: ${financiamiento}\nForma de pago: ${formapago}\nEnganche: ${enganche}\nFavor de tener copia legible de la identificación y original de la misma para la entrega.\n\nLa guía la puede monitorear en https://alfmensajeria.com.mx`;
                                }

                                if (guia != '' && statusofrequest == 'Guia enviada'){
                                    client.messages.create({
                                         from: 'whatsapp:+13017109139',
                                         body: mensajeWhats,
                                         to: "whatsapp:+521"+contacto
                                         //to: "whatsapp:+5219991435205"
                                       })
                                        .then(message => {
                                            
                                            logs.feed_syslogs(
                                                  req.body.iduser,
                                                  message.status,
                                                    message.body,
                                                    message.to,
                                                    `${guia} - ${folio} - ${equipo} - ${financiamiento} - ${formapago}`,
                                                    "Actualizacion GUIA EAD por cerrador a domicilio",
                                                    req.connection.remoteAddress,
                                                    message.price,
                                                    message.priceUnit,
                                                    message.from,
                                                    message.dateCreated,
                                                    message.dateUpdated,
                                                    message.dateSent,
                                                    message.direction,
                                                    message.errorCode,
                                                    message.errorMessage,
                                                    message.messagingServiceSid,
                                                    message.numMedia,
                                                    message.numSegments,
                                                    message.sid
                                                  );
                                        })
                                        .catch(e => { console.error('Got an error:', e.code, e.message); });
                                        ;
                                        
                                }


                            });//fin del commit

                        });//fin de la tercer query
                    });//fin de la query 2
                });//fin de la query 1
            }); //fin del begin transaction
            pool.release();
        }); //fin del get connection
    });

    //GENERAR REPORTE DE DOMICILIO 
    app.post('/getDataDomicilio', middleware.requireLogin, function (req, res) {
        var fromdate = req.body.fromdate;
        var tildate = req.body.tildate;
        var response = [];
        var query = `SELECT m.enganche,date_format(m.created,"%Y-%m-%d %h:%i") as created, m.guidenumber, concat(u.name," ",u.lastname) as asesor , s.clientname,s.mainline, s.contact,d.sisact,p.keyname, p.name, s.city,d.account,date_format(m.activation_date,"%Y-%m-%d %h:%i") as activation_date,d.phone,m.invoice,if(d.imei is not null,concat("'",d.imei),'') as imei,m.iccid,date_format(m.datewithprovider,"%Y-%m-%d %h:%i") as datewithprovider,date_format(m.delivered_date,"%Y-%m-%d %h:%i") as delivered_date,m.status,d.type from metashipping m left join user u ON m.iduser = u.iduser left join shipping s on m.idshipping = s.idshipping LEFT JOIN datashipping d ON m.idshipping = d.idshipping LEFT JOIN plan p ON d.idplan = p.idplan WHERE created BETWEEN ? AND ? OR datewithprovider BETWEEN ? AND ?`;
        var inserts = [fromdate, tildate, fromdate, tildate];
        query = mysql.format(query,inserts);

        connection.getConnection(function (err, conn) {
            connection.query(query, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'creado': element.created,
                        'guia': element.guidenumber,
                        'asesor': element.asesor,
                        'nombre cliente': element.clientname,
                        'telefono': element.mainline,
                        'contacto': element.contact,
                        'sisact': element.sisact,
                        'clave': element.keyname,
                        'plan': element.name,
                        'ciudad': element.city,
                        'cuenta': element.account,
                        'fecha de activacion': element.activation_date,
                        'equipo': element.phone,
                        'factura': element.invoice,
                        'enganche': element.enganche,
                        'imei': element.imei,
                        'iccid': element.iccid,
                        'fecha de envio': element.datewithprovider,
                        'fecha de entrega': element.delivered_date,
                        'estatus': element.status,
                        'tipo': element.type,
                    }
                    response.push(row);
                });
                res.send(response);
            });
            conn.release();
        });
        
    });


}// fin del archivo
