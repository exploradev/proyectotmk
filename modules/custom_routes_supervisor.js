middleware = require('./middlewares');
var mysql = require('mysql');
var connection = require('./db');
var moment = require('moment');


module.exports = function(app,io){

    


    //OPERACIONES PARA ALIMENTAR TABLAS DE USUARIOS PARA EL COORDINADOR-----------

    app.post('/listausuarios', middleware.requireLogin, function (req, res) {

        var statistics_data = [];
        var usuarios = "SELECT user.iduser AS numempleado, CONCAT(user.lastname, ' ' ,user.name) AS usuario, user.username AS username, rol.name AS rol, campaign.name AS campaign, user.added AS fechaingreso, user.active AS activo from user left join rol ON user.idrol = rol.idrol left join campaign ON campaign.idcampaign = user.idcampaign ORDER BY usuario ASC";

        connection.getConnection(function (err, conn) {
            conn.query(usuarios, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'numempleado': element.numempleado,
                        'usuario': element.usuario,
                        'username': element.username,
                        'rol': element.rol,
                        'campaign': element.campaign,
                        'fechaingreso': moment(element.fechaingreso).format('DD[/]MM[/]YYYY'),
                        'activo': element.activo
                    }
                    statistics_data.push(row);
                });
                res.send(statistics_data);
            });
            conn.release();
        });
        
    }); //fin del /listausuarios

    app.post('/listaplanes', middleware.requireLogin, function (req, res) {

        var planes = [];
        var queryplanes = "SELECT * FROM plan where idplan != 1";

        connection.getConnection(function (err, conn) {
            conn.query(queryplanes, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'nombre': element.name,
                        'clave': element.keyname,
                        'plazo': element.term,
                        'activo': element.active,
                        'idplan': element.idplan,
                        'renta': element.renta
                    }
                    planes.push(row);
                });
                res.send(planes);
            });
            conn.release();
        });
        
    }); //fin del /listausuarios

    app.post('/listacapturas', middleware.requireLogin, function (req, res) {

        var inicio = req.body.inicio;
        var fin = req.body.fin;
        var status = req.body.status;
        var planes = [];
        var requests = "SELECT request.idrequest as idrequest, request.created AS creado, CONCAT(datacustomer.lastname,' ',datacustomer.name) AS cliente, CONCAT(user.lastname,' ',user.name) AS vendedor, request.channel AS canal, request.sisact AS sisact,request.status AS status FROM request LEFT JOIN datacustomer ON datacustomer.idrequest = request.idrequest LEFT JOIN user ON request.iduser = user.iduser WHERE date(created) between ? and ? and request.status = ?";

        var inserts = [inicio,fin,status];
        var query_requests = mysql.format(requests, inserts);

        connection.getConnection(function (err, conn) {
            conn.query(query_requests, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'idrequest': element.idrequest,
                        'creado': moment(element.creado).format('DD[-]MM[ ]HH:MM'),
                        'cliente': element.cliente,
                        'vendedor': element.vendedor,
                        'sisact': element.sisact,
                        'canal': element.canal,
                        'status': element.status
                    }
                    planes.push(row);
                });
                res.send(planes);
            });
            conn.release();
        });
        
    }); //fin del /listausuarios

    //helper para alimentar select para asignar usuarios
    app.post('/selectusers', middleware.requireLogin, function (req, res) {

        var statistics_data = [];
        var usuarios = "SELECT user.iduser AS iduser, CONCAT(user.lastname, ' ' ,user.name) AS asesor from user WHERE user.active = 1 ORDER BY asesor ASC";

        connection.getConnection(function (err, conn) {
            conn.query(usuarios, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'iduser': element.iduser,
                        'asesor': element.asesor
                    }
                    statistics_data.push(row);
                });
                res.send(statistics_data);
            });
            conn.release();
        });
        
    }); //fin del /listausuarios

    //helper para borrar capturas
    app.post('/deleterequest', middleware.requireLogin, function (req, res) {

        var idrequest = req.body.idrequest;  
        var usuarios = "DELETE FROM request WHERE idrequest = ? ";
        var inserts = [idrequest];
        var query_user = mysql.format(usuarios, inserts);

        connection.getConnection(function (err, conn) {
            conn.query(query_user, function (error, results, field) {
                if (error) throw error;

                res.send('deleted');
            });
            conn.release();
        });
        
        console.log("Eliminado registro " + idrequest);
    });

    //helper para reasignar capturas
    app.post('/reasignrequest', middleware.requireLogin, function (req, res) {
        
        var idrequest = req.body.idrequest;   
        var iduser = req.body.iduser;         
        var usuarios = "UPDATE request SET iduser = ? WHERE idrequest = ?";
        var inserts = [iduser,idrequest];
        var query_user = mysql.format(usuarios, inserts);

        connection.getConnection(function (err, conn) {
            conn.query(query_user, function (error, results, field) {
                if (error) throw error;
                res.send('updated');
            });
            conn.release();
        });
        

        console.log("Cambio de propietario por coordinador de captura: " + idrequest + " - asignado a: " + iduser);
    }); //fin de reasignacion

    //helper para edicion de la tabla de planes

    app.post('/setnewdataplan', middleware.requireLogin, function(req,res){
        idrequest = req.body.idrequest;
        modificacion = req.body.modificacion;
        atributo = req.body.atributo;

        if(atributo == 'nombreplan'){
            var query = "UPDATE plan SET name = ? WHERE idplan = ?";
        } else if (atributo == 'claveplan') {
            var query = "UPDATE plan SET keyname = ? WHERE idplan = ?";
        } else if(atributo == 'plazoplan'){
            var query = "UPDATE plan SET term = ? WHERE idplan = ?";
        } else if (atributo == 'statusplan') {
            var query = "UPDATE plan SET active = ? WHERE idplan = ?";
        } else if (atributo == 'rentaplan') {
            var query = "UPDATE plan SET renta = ? WHERE idplan = ?";
        }


        var inserts = [modificacion, idrequest];
        var query_plan = mysql.format(query, inserts);

        connection.getConnection(function (err, conn) {
            conn.query(query_plan, function (error, results, field) {
                if (error) throw error;
                res.send('updated');
                if (atributo == 'nombreplan') {
                    console.log("Modificacion de plan: " + idrequest + " - Nuevo nombre: " + modificacion);
                } else if (atributo == 'claveplan') {
                    console.log("Modificacion de plan: " + idrequest + " - Nueva clave: " + modificacion);
                } else if (atributo == 'plazoplan') {
                    console.log("Modificacion de plan: " + idrequest + " - Nuevo plazo: " + modificacion);
                } else if (atributo == 'statusplan') {
                    console.log("Modificacion de plan: " + idrequest + " - Nuevo status: " + modificacion);
                } else if (atributo == 'rentaplan') {
                    console.log("Modificacion de plan: " + idrequest + " - Nueva renta: " + modificacion);
                }
            });
            conn.release();
        });
        
        


       
        
    });

    app.post('/insertplan', middleware.requireLogin, function (req, res) {
        name = req.body.name;
        keyname = req.body.keyname;
        term = req.body.term;
        active = req.body.active;

        
        var query = "INSERT INTO plan(name,keyname,term,active) VALUES(?,?,?,?)";
        var inserts = [name, keyname, term, active];

        var query_plan = mysql.format(query, inserts);

        connection.getConnection(function (err, conn) {
            conn.query(query_plan, function (error, results, field) {
                if (error) throw error;
                res.send('updated');
                console.log("Se agrego nuevo plan: " + results.insertId);
            });
            conn.release();
        });
        
    });



    //deprecated route------------------------
    app.post('/busquedausuarios', middleware.requireLogin, function (req, res) {

        var busqueda = req.body.busqueda;
        var buscarpor = req.body.buscarpor;

        var statistics_data = [];

        if (buscarpor == 'usuario'){
            var usuarios = "SELECT user.iduser AS numempleado, CONCAT(user.lastname, ' ' ,user.name) AS usuario, user.username AS username, rol.name AS rol, campaign.name AS campaign, user.added AS fechaingreso from user left join rol ON user.idrol = rol.idrol left join campaign ON campaign.idcampaign = user.idcampaign WHERE user.username LIKE ? ORDER BY user.username ASC";
        }else if(buscarpor = 'apellidos'){
            var usuarios = "SELECT user.iduser AS numempleado, CONCAT(user.lastname, ' ' ,user.name) AS usuario, user.username AS username, rol.name AS rol, campaign.name AS campaign, user.added AS fechaingreso from user left join rol ON user.idrol = rol.idrol left join campaign ON campaign.idcampaign = user.idcampaign WHERE user.lastname LIKE ? ORDER BY user.lastname ASC";
        }
        
        var inserts = ['%'+busqueda+'%'];
        var query_user = mysql.format(usuarios, inserts);
        

        connection.getConnection(function (err, conn) {
            conn.query(query_user, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'numempleado': element.numempleado,
                        'usuario': element.usuario,
                        'username': element.username,
                        'rol': element.rol,
                        'campaign': element.campaign,
                        'fechaingreso': moment(element.fechaingreso).format('DD[/]MM[/]YYYY')
                    }
                    statistics_data.push(row);
                });
                res.send(statistics_data);

            });
            conn.release();
        });
        
    }); //fin del /busqueda
    //deprecated route------------------------




    app.post('/getuserdata', middleware.requireLogin, function (req, res) {

        var user_data = [];
        var userid = req.body.userid;
        var usuario = "SELECT iduser, name, lastname, username, idrol, idcampaign, shift, active FROM user WHERE iduser = ? LIMIT 1";
        var inserts = [userid];
        var query_user = mysql.format(usuario, inserts);
        
        connection.getConnection(function(err,conn){
            conn.query(query_user, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'userid': element.iduser,
                        'name': element.name,
                        'lastname': element.lastname,
                        'username': element.username,
                        'idrol': element.idrol,
                        'idcampaign': element.idcampaign,
                        'shift': element.shift,
                        'active': element.active
                    }
                    user_data.push(row);
                });
                res.send(user_data);
            });
            conn.release();
        });
        
    }); //fin del /listausuarios

    app.post('/userInsertUpdate', middleware.requireLogin, function (req, res) {
        

        var action = req.body.action;
        var userid = req.body.userid;
        var edit_nombres = req.body.edit_nombres;
        var edit_apellidos = req.body.edit_apellidos;
        var edit_username = req.body.edit_username;
        var edit_password = req.body.edit_password;
        var edit_rol = req.body.edit_rol;
        var edit_campania = req.body.edit_campania;
        var edit_turno = req.body.edit_turno;
        var edit_estatus = req.body.edit_estatus;
        
        
        var last_id_inserted;
        connection.getConnection(function (err, pool) {
            pool.beginTransaction(function (err) {
                if (err) throw err;

                //se detecta la acction insert o update
                //se valida si se ingreso password al momento de editar y se toman acciones 

                if(action == 'insert'){
                    var insert_request = "INSERT INTO user(name,lastname,username,idrol,password,active,idcampaign,shift) VALUES(?,?,?,?,?,?,?,?)";
                    var inserts = [
                        edit_nombres,
                        edit_apellidos,
                        edit_username,
                        edit_rol,
                        edit_password,
                        edit_estatus,
                        edit_campania,
                        edit_turno
                    ];
                }else if(action == 'update'){
                    if((edit_password == null)||(edit_password == '')){
                        var insert_request = "UPDATE user SET name = ?, lastname = ?,username = ?,idrol = ?,active = ?,idcampaign = ?,shift = ? WHERE  iduser = ?";
                        var inserts = [
                            edit_nombres,
                            edit_apellidos,
                            edit_username,
                            edit_rol,
                            edit_estatus,
                            edit_campania,
                            edit_turno,
                            userid
                        ];
                    }else{
                        var insert_request = "UPDATE user SET name = ?, lastname = ?,username = ?,idrol = ?,password = ?,active = ?,idcampaign = ?,shift = ? WHERE  iduser = ?";
                        var inserts = [
                            edit_nombres,
                            edit_apellidos,
                            edit_username,
                            edit_rol,
                            edit_password,
                            edit_estatus,
                            edit_campania,
                            edit_turno,
                            userid
                        ];
                    }
                }


                
                
                query_request = mysql.format(insert_request, inserts);

                
                pool.query(query_request, function (err, result) {
                    if (err) {
                        pool.rollback(function () {
                            throw err;
                        });
                    }
                    last_id_inserted = result.insertId;
                    pool.commit(function (err) {
                        if (err) {
                            pool.rollback(function () {
                                throw err;
                            });
                        }
                        if (last_id_inserted == 0){
                            console.log('Se actualizo usuario con ID: ' + userid);
                        }else{
                            console.log('Se agrego nuevo usuario con ID: ' + last_id_inserted);
                        }
                        
                        res.send('exito');
                    });//fin del commit
                });//fin del query a tabla request <------
            });// fin de beginTransaction
            pool.release();
        }); //fin de getConnection                   
    }); //fin de /userInsertUpdate

    //VALIDACION DE EXISTENCIA DE USERNAME PARA EVITAR DUPLICIDAD EN DB
    app.post('/validateDuplicatedUsername', middleware.requireLogin, function (req, res) {

        var value = req.body.value;

        var usuarios = "SELECT SUM(CASE WHEN user.username = ? THEN 1 ELSE 0 END) AS duplicidad FROM user";
        

        var inserts = [value];
        var query_user = mysql.format(usuarios, inserts);

        
        connection.getConnection(function(err,conn){
            conn.query(query_user, function (error, results, field) {
                if (error) throw error;
                results.forEach(element => {
                    if (element.duplicidad == 0) {
                        res.send('unico');
                    } else {
                        res.send('duplicado');
                    }
                });
            });
            conn.release();
        });
        
    }); //fin del /validateDuplicatedUsername
    
    //GRAFICAS SUPERVISOR

    app.post('/getDashboardSevenDays', middleware.requireLogin, function (req, res) {
        var statistics_data = [];
        var conteo_excelentes = "SELECT        SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 6 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus6,            SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 5 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus5,                SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 4 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus4,                    SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 3 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus3,                        SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 2 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus2,                            SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 1 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus1,                                SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 0 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS minus0        from request LEFT JOIN user on request.iduser = user.iduser LEFT JOIN campaign on user.idcampaign = campaign.idcampaign";

        var conteo_estrena = "SELECT        SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 6 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus6,            SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 5 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus5,                SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 4 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus4,                    SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 3 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus3,                        SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 2 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus2,                            SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 1 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus1,                                SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = DATE_ADD(CURDATE(), INTERVAL - 0 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS minus0        from request LEFT JOIN user on request.iduser = user.iduser LEFT JOIN campaign on user.idcampaign = campaign.idcampaign"


        
        connection.getConnection(function(err,pool){
            pool.query(conteo_excelentes, function (error, results, field) {
                if (error) throw error;


                statistics_data.push(results[0].minus6);
                statistics_data.push(results[0].minus5);
                statistics_data.push(results[0].minus4);
                statistics_data.push(results[0].minus3);
                statistics_data.push(results[0].minus2);
                statistics_data.push(results[0].minus1);
                statistics_data.push(results[0].minus0);

                pool.query(conteo_estrena, function (error, results, field) {
                    if (error) throw error;


                    statistics_data.push(results[0].minus6);
                    statistics_data.push(results[0].minus5);
                    statistics_data.push(results[0].minus4);
                    statistics_data.push(results[0].minus3);
                    statistics_data.push(results[0].minus2);
                    statistics_data.push(results[0].minus1);
                    statistics_data.push(results[0].minus0);



                    res.send(statistics_data);
                });

            });
            pool.release();
        });
        
    });// fin de /getDashboardSevenDays

    app.post('/getDashboardSevenDaysCapturas', middleware.requireLogin, function (req, res) {
        var statistics_data = [];
        var conteo_excelentes = "SELECT SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 6 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus6,            SUM(CASE WHEN request.status != 'borrador' AND date(request. created) = DATE_ADD(CURDATE(), INTERVAL - 5 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus5,                SUM(CASE WHEN request.status != 'borrador' AND date(request. created) = DATE_ADD(CURDATE(), INTERVAL - 4 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus4,                    SUM(CASE WHEN request.status != 'borrador' AND date(request. created) = DATE_ADD(CURDATE(), INTERVAL - 3 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus3,                        SUM(CASE WHEN request.status != 'borrador' AND date(request. created) = DATE_ADD(CURDATE(), INTERVAL - 2 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus2,                            SUM(CASE WHEN request.status != 'borrador' AND date(request. created) = DATE_ADD(CURDATE(), INTERVAL - 1 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus1,                                SUM(CASE WHEN request.status != 'borrador' AND date(request. created) = DATE_ADD(CURDATE(), INTERVAL - 0 DAY) AND campaign.idcampaign = 1 THEN 1 ELSE 0 END) AS cminus0        from request LEFT JOIN user on request.iduser = user.iduser LEFT JOIN campaign on user.idcampaign = campaign.idcampaign";

        var conteo_estrena = "SELECT SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 6 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus6, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 5 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus5, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 4 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus4, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 3 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus3, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 2 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus2, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 1 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus1, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = DATE_ADD(CURDATE(), INTERVAL - 0 DAY) AND campaign.idcampaign = 2 THEN 1 ELSE 0 END) AS cminus0        from request LEFT JOIN user on request.iduser = user.iduser LEFT JOIN campaign on user.idcampaign = campaign.idcampaign";

        connection.getConnection(function (err, pool) {
            pool.query(conteo_excelentes, function (error, results, field) {
                if (error) throw error;


                statistics_data.push(results[0].cminus6);
                statistics_data.push(results[0].cminus5);
                statistics_data.push(results[0].cminus4);
                statistics_data.push(results[0].cminus3);
                statistics_data.push(results[0].cminus2);
                statistics_data.push(results[0].cminus1);
                statistics_data.push(results[0].cminus0);

                pool.query(conteo_estrena, function (error, results, field) {
                    if (error) throw error;


                    statistics_data.push(results[0].cminus6);
                    statistics_data.push(results[0].cminus5);
                    statistics_data.push(results[0].cminus4);
                    statistics_data.push(results[0].cminus3);
                    statistics_data.push(results[0].cminus2);
                    statistics_data.push(results[0].cminus1);
                    statistics_data.push(results[0].cminus0);

                    res.send(statistics_data);
                });
            });
            pool.release();
        });
        
    });// fin de /getDashboardSevenDaysCapturas
    
    app.post('/getDataLeftDashboard', middleware.requireLogin, function (req, res) {
        var statistics_data = [];
        var conteo = "SELECT SUM(CASE WHEN request.status = 'activa' and month(request.activated) = month(now()) THEN 1 ELSE 0 END) AS activaciones, SUM(CASE WHEN request.status != 'borrador' and month(request.created) = month(now()) THEN 1 ELSE 0 END) AS capturas, SUM(CASE WHEN request.status IN('rechazada', 'no finalizada')  and month(request.created) = month(now()) THEN 1 ELSE 0 END) AS rechazos, SUM(CASE WHEN request.status = 'activa' AND date(request.activated) = date(now()) THEN 1 ELSE 0 END) AS activacionesdia, SUM(CASE WHEN request.status != 'borrador' AND date(request.created) = date(now()) THEN 1 ELSE 0 END) AS capturasdia, SUM(CASE WHEN request.status IN('rechazada', 'no finalizada') AND date(request.created) = date(now()) THEN 1 ELSE 0 END) AS rechazosdia, SUM(CASE WHEN request.status = 'aceptada' AND date(request.created) = date(now()) THEN 1 ELSE 0 END) AS aceptadasdia FROM request";

        connection.getConnection(function (err, conn) {
            conn.query(conteo, function (error, results, field) {

                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'activaciones': element.activaciones,
                        'capturas': element.capturas,
                        'rechazos': element.rechazos,
                        'activacionesdia': element.activacionesdia,
                        'capturasdia': element.capturasdia,
                        'rechazosdia': element.rechazosdia,
                        'aceptadasdia': element.aceptadasdia
                    }
                    statistics_data.push(row);
                });
                res.send(statistics_data);
            });
            conn.release();
        });
        
    });// fin de /getDataLeftDashboard

    app.post('/getDataAgentDashboard', middleware.requireLogin, function (req, res) {
        var statistics_data = [];
        var conteo = "SELECT        CONCAT(user.lastname, ' ', user.name) AS vendedor,            SUM(CASE WHEN request.status != 'borrador' and month(request.created) = month(now()) THEN 1 ELSE 0 END) AS capturas,                SUM(CASE WHEN request.status = 'activa' and month(request.activated) = month(now()) THEN 1 ELSE 0 END) AS activadas,                    SUM(CASE WHEN request.status IN('rechazada', 'no finalizada')  and month(request.created) = month(now()) THEN 1 ELSE 0 END) AS nofinalizadas        FROM request        LEFT JOIN user ON request.iduser = user.iduser        WHERE user.active = 1        GROUP BY vendedor        ORDER BY capturas DESC;";

        connection.getConnection(function (err, conn) {
            conn.query(conteo, function (error, results, field) {

                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'vendedor': element.vendedor,
                        'capturas': element.capturas,
                        'activadas': element.activadas,
                        'nofinalizadas': element.nofinalizadas

                    }
                    statistics_data.push(row);
                });
                res.send(statistics_data);
            });
            conn.release();
        });
        
    });// fin de /getDataAgentDashboard

    //ROUTE PARA DATOS DEL CICLO DE VIDA DE LAS CAPTURAS EN: custom_routes_analista.js



    //REPORTES
    app.post('/generateReport', middleware.requireLogin, function (req, res) {

        fechainicio = req.body.fechainicio;
        fechafin = req.body.fechafin;
        statistics_data = [];
        var reporte = "SELECT request.curp, request.created AS capturada, campaign.name AS campania,       request.sisact AS sisact, datacustomer.contact_num AS numero,               dataplan.migrate_num AS migracion,  dataplan.ref_account AS ctamobile,                  request.channel AS canal, CONCAT(datacustomer.lastname, ' ', datacustomer.name) AS cliente,  plan.name AS plan,  plan.term AS plazo,    CONCAT(user.lastname, ' ', user.name) AS vendedor,   request.activated AS activada,                                request.status AS status,                                                            dataplan.celphone AS equipo,                                                                TIME_FORMAT(TIMEDIFF(request.activated, request.created), '%k:%i:%s') AS tiempoenactivarglobal, TIME_FORMAT(TIMEDIFF(request.activated, request.captured), '%k:%i:%s') AS activadadesdesisact, TIME_FORMAT(TIMEDIFF(request.captured, request.created), '%k:%i:%s') AS sisactdesdecreacion, TIME_FORMAT(TIMEDIFF(request.firststatus, request.captured), '%k:%i:%s') AS seguimientodesdesisact FROM request        LEFT JOIN datacustomer ON datacustomer.idrequest = request.idrequest        LEFT JOIN dataplan ON dataplan.idrequest = request.idrequest        LEFT JOIN user ON request.iduser = user.iduser      LEFT JOIN campaign ON user.idcampaign = campaign.idcampaign        LEFT JOIN plan ON dataplan.idplan = plan.idplan        WHERE date(created) between ? AND ?";

        var inserts = [fechainicio, fechafin];
        
        var query_report = mysql.format(reporte, inserts);

        connection.getConnection(function (err, conn) {
            conn.query(query_report, function (error, results, field) {

                if (error) throw error;
                results.forEach(element => {
                    var row = {
                        'capturada': moment(element.capturada).format('YYYY[-]MM[-]DD[ ]HH:MM:ss'),
                        'campania': element.campania,
                        'sisact': element.sisact,
                        'numero': element.numero,
                        'migracion': element.migracion,
                        'ctamobile': element.ctamobile,
                        'canal': element.canal,
                        'cliente': element.cliente,
                        'curp': element.curp,
                        'plan': element.plan,
                        'plazo': element.plazo,
                        'vendedor': element.vendedor,
                        'activada': moment(element.activada).format('YYYY[-]MM[-]DD[ ]HH:MM:ss'),
                        'status': element.status,
                        'equipo': element.equipo,
                        'creacion de sisact': element.sisactdesdecreacion,
                        'seguimiento despues de creacion de sisact': element.seguimientodesdesisact,
                        'activa despues de creacion de sisact': element.activadadesdesisact,
                        'tiempo en activar global': element.tiempoenactivarglobal
                    }
                    statistics_data.push(row);
                });
                res.send(statistics_data);
            });
            conn.release();
        });
        
    });// fin de /generateReport

    

    app.post('/reporte_capturas_telmex', middleware.requireLogin, (req, res) => {
        let {fechainicio,fechafin} = req.body;
        let query = `SELECT * from telmex_activaciones WHERE date(creado) between date(?) and date(?)`;
        let inserts = [fechainicio,fechafin];
        query = mysql.format(query,inserts);
        connection.getConnection(function (err, conn) {
            let connection_error = {
                status: 'error',
                mensaje: 'Error al conectar con la base de datos'
            }
            if (err) res.send(connection_error)
            conn.query(query, function (error, results, field) {
                results.forEach((element) => {
                    if (element.creado) {
                      element.creado = moment(element.creado).format(
                        "DD[/]MM[/]YYYY[ ]HH[:]mm[:]ss"
                      );
                    }
                });
                res.send(results)
            });
            conn.release();
        });
    })
    
    app.post('/reporte_portabilidad_telmex', middleware.requireLogin, (req, res) => {
        let {fechainicio,fechafin} = req.body;
        let query = `SELECT * from telmex_portabilidad WHERE date(creado) between date(?) and date(?)`;
        let inserts = [fechainicio,fechafin];
        query = mysql.format(query,inserts);
        connection.getConnection(function (err, conn) {
            let connection_error = {
                status: 'error',
                mensaje: 'Error al conectar con la base de datos'
            }
            if (err) res.send(connection_error)
            conn.query(query, function (error, results, field) {
                results.forEach((element) => {
                    if (element.creado) {
                      element.creado = moment(element.creado).format(
                        "DD[/]MM[/]YYYY[ ]HH[:]mm[:]ss"
                      );
                    }
                });
                res.send(results)
            });
            conn.release();
        });
    })

}// fin del archivo