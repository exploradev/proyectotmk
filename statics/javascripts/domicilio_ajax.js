var selectplan;
$(document).ready(function(){

    //-----------------------------------------------------------------------
    //-------------------------------WEBSOCKETS------------------------------
    //-----------------------------------------------------------------------

    socket = io.connect('http://'+location.host);
    console.log("websocket addr: http://"+location.host)

    socket.on('refreshalldivs', function (msg) {
        console.log('Actualizando divs');
    });

    socket.on('actualizartablasdomicilio', function (msg) {
        //escucha:
        

        //capturo la pestaña activa para actualizarla
        //con un trigger click
        $('.linktabactive').trigger('click');
        fillcounters();
        console.log('websocketcambio')
    });

    //-----------------------------------------------------------------------
    //-------------------------------WEBSOCKETS------------------------------
    //-----------------------------------------------------------------------


   /*
    STATUS DE CAPTURAS
    ------------------
    Cambio a sinergia
    Cancelada/Err.Mensajeria
    Equipo no disponible
    Guia entregada
    pendiente
    Cancelada por cliente
    Cliente no contesta
    Err.Asesor/Nombre erroneo
    Guia enviada
    Trámite incompleto
    Cancelada/Err.Asesor
    Confirmado/Pend.Envío
    Err.Asesor/No tiene ID
    No cuenta con pago
    

   */


    //LLENADO DE LAS OPCIONES DEL SELECT2 DE PLANES
    selectplan = function () {
        //data-clave y data-plazo
        //import select options from ajax 
        $.post('/selectoptions', function (response) {
            //var table_body = [];
            var table_body = '<option></option>';
            for (i = 0; i < response.length; i++) {
                table_body += "<option value ='" + response[i]["idplan"] + "' data-keyname = '" + response[i]["keyname"] + "' data-term = '" + response[i]["term"] + "'>";
                table_body += response[i]["name"];
                table_body += "</option>";
            }
            $('#detalles_plan').html(table_body);
        });
    }

    //OBTENER DETALLES DEL PLAN AL MOMENTO DE QUE EL SELECT CAMBIE DE PLAN
    $("#detalles_plan").change(function () {
        //datos del plan en confirmacion de captura
        
        var claveplan = $(this).find(":selected").data("keyname");
        var plazoplan = $(this).find(":selected").data("term");
        

        if (claveplan == undefined) {
            $('#detalles_clave').html('Clave:');
            $('#detalles_plazo').html('Plazo:');
        } else {
            $('#detalles_clave').html(claveplan);
            $('#detalles_plazo').html(plazoplan);
        }
    });

    
    //AL CLICKEAR SE HACE LA ACTUALIZACION
    $('#btn_comnt').click(function(){
        //OBTENER LOS DATOS DEL FORMULARIO DE DETALLES DE CAPTURAS EN PERFIL DE ANALISTA
        var idshipping = $('#mdom_id').html();

        var cliente = $('#detalles_cliente').val();
        var contacto = $('#detalles_contacto').val();
        var numamigrar = $('#detalles_numamigrar').val();
        var destino = $('#detalles_destino').val();
        var fechaentrega = $('#detalles_fechaentrega').val();
        var horarioentrega = $('#detalles_horaentrega').val();
        var direccion = $('#detalles_direccion').val();
        var colonia = $('#detalles_colonia').val();
        var referencias = $('#detalles_referencias').val();
        var enganche = $('#detalles_enganche').val();

        var folio = $('#detalles_sisact').val();
        var canal = $('#detalles_canal').val();
        var plan = $('#detalles_plan').val();
        var tipoactivacion = $('#detalles_tipo').val();
        var ctaconsolidar = $('#detalles_ctaconsolidar').val();
        var email = $('#detalles_email').val();
        var equipo = $('#detalles_equipo').val();
        var imei = $('#detalles_imei').val();
        var financiamiento = $('#detalles_financiemiento').val();
        var concepto = $('#detalles_pagara').val();
        var observaciones = $('#detalles_observaciones').val();

        var factura = $('#detalles_factura').val();
        var iccid = $('#detalles_iccid').val();
        var guia = $('#detalles_guia').val();
        var proveedor = $('#detalles_proveedor').val();
        var activacion = $('#detalles_fechaactivacion').val();
        var envio = $('#detalles_fechaenvio').val();
        var entregado = $('#detalles_fechaentregado').val();
        var observacionanalista = $('#observacionstatus').val();

        var formapago = $('#detalles_formapago').val();

        var statusofrequest = $('#mdom_status').val();
        iduser = $('body').attr('data-iduser');
        $.post('/updatedatadomicilio',
        {
            iduser: iduser,
            idshipping:idshipping,
            cliente:cliente,
            contacto:contacto,
            numamigrar:numamigrar,
            destino:destino,
            fechaentrega:fechaentrega,
            horarioentrega:horarioentrega,
            direccion:direccion,
            colonia:colonia,
            referencias:referencias,
            enganche:enganche,
            folio:folio,
            canal:canal,
            plan:plan,
            tipoactivacion:tipoactivacion,
            ctaconsolidar:ctaconsolidar,
            email:email,
            equipo:equipo,
            imei:imei,
            financiamiento:financiamiento,
            concepto:concepto,
            observaciones:observaciones,
            factura:factura,
            iccid:iccid,
            guia:guia,
            proveedor:proveedor,
            activacion:activacion,
            envio:envio,
            entregado:entregado,
            observacionanalista:observacionanalista,
            statusofrequest:statusofrequest,
            formapago: formapago
        }
        ,function(response){
            if(response == 'Correcto'){
                socket.emit('setWebsocketUpdate', { route: 'actualizardomicilio', idrequest: idshipping });
                alert('Captura '+idshipping+' actualizada correctamente.');
                //AGREGAR AQUI EL EMIT DE CAMBIO REALIZADO
               
            }else{
                alert('Error al actualizar');
            }
        });

    });

    //FUNCION QUE LLENA LA TABLA DE EXPORTACION SEGUN LOS PARAMETROS DE FECHA
    $('#btn_exporttable').click(function(){
        var fromdate = $('#fromdate').val();
        var tildate = $('#tildate').val();

        $.post('/getDataDomicilio',{fromdate:fromdate,tildate:tildate},function(response){
            if (response == ''){
                console.log("Sin resultados o tabla incorrecta - Defensa de XSS");
                alert("Sin resultados");
                return;
            }else{
                JSONToCSVConvertor(response, "EAD", true);
            }
        });
    });

    ///////////////////////////////////////////////////////////////
    //funcion para exportar reportes en excel
    //jsfiddle: http://jsfiddle.net/hybrid13i/JXrwM/
    function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
        //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
        var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

        var CSV = '';
        //Set Report title in first row or line

        CSV += ReportTitle + '\r\n\n';

        //This condition will generate the Label/Header
        if (ShowLabel) {
            var row = "";

            //This loop will extract the label from 1st index of on array
            for (var index in arrData[0]) {

                //Now convert each value to string and comma-seprated
                row += index + ',';
            }

            row = row.slice(0, -1);

            //append Label row with line break
            CSV += row + '\r\n';
        }

        //1st loop is to extract each row
        for (var i = 0; i < arrData.length; i++) {
            var row = "";

            //2nd loop will extract each column and convert it in string comma-seprated
            for (var index in arrData[i]) {
                row += '"' + arrData[i][index] + '",';
            }

            row.slice(0, row.length - 1);

            //add a line break after each row
            CSV += row + '\r\n';
        }

        if (CSV == '') {
            alert("Invalid data");
            return;
        }

        //Generate a file name
        var fileName = "Reporte_";
        //this will remove the blank-spaces from the title and replace it with an underscore
        fileName += ReportTitle.replace(/ /g, "_");

        //Initialize file format you want csv or xls
        var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

        // Now the little tricky part.
        // you can use either>> window.open(uri);
        // but this will not work in some browsers
        // or you will not get the correct file extension    

        //this trick will generate a temp <a /> tag
        var link = document.createElement("a");
        link.href = uri;

        //set the visibility hidden so it will not effect on your web-layout
        link.style = "visibility:hidden";
        link.download = fileName + ".csv";

        //this part will append the anchor tag and remove it after automatic click
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }
    ////////////////////////////////////////////////////////////////


    //se llena el select de las opciones de planes al cargar el dom por primera vez
    selectplan();
    
});