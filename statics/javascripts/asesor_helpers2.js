var resetfields2;
$(document).ready(function () {

    //JS creado para gestionar unicamente las acciones del modal
    //de entrega a domicilio

    //EN ESTE JS:
    //resetfields2 elimina campos del modal
    
    
    resetfields2 = function() {
        

        //pestaña datos para entrega
        $("input[name='dom_cliente']").val('');
        $("input[name='dom_fechaentrega']").val('');
        $("input[name='dom_horarioentrega']").val('');
        $("input[name='dom_contacto']").val('');
        $("input[name='dom_numamigrar']").val('');
        $("input[name='dom_destino']").val('');
        $("input[name='dom_direccion']").val('');
        $("input[name='dom_colonia']").val('');
        $("input[name='dom_referencias']").val('');

        //pestaña datos del tramite
        $("input[name='dom_folio']").val('');
        $('#dom_canal').val('');
        $('#dom_plan').val('');
        $('#dom_plan').trigger('change');
        $("select[name='dom_tipoactivacion']").val('default');
        $("input[name='dom_ctaconsolidar']").val('');
        $("input[name='dom_email']").val('');
        $("input[name='dom_financiamiento']").val('');
        $("input[name='dom_enganche']").val('');
        $("input[name='dom_equipo']").val('');
        $("input[name='dom_imei']").val('');
        $("input[name='dom_concepto']").val('');

        //pestaña confirmacion de datos

        $("#conf_dom_cliente").html('');
        $("#conf_dom_fechaentrega").html('');
        $("#conf_dom_horarioentrega").html('');
        $("#conf_dom_contacto").html('');
        $("#conf_dom_numamigrar").html('');
        $("#conf_dom_destino").html('');
        $("#conf_dom_direccion").html('');
        $("#conf_dom_colonia").html('');
        $("#conf_dom_referencias").html('');

        $("#conf_dom_folio").html('');
        $('#conf_dom_canal').html('');
        $('#conf_dom_plan').html('');
        $("#conf_dom_claveplan").html('');
        $("#conf_dom_plazoplan").html('');
        $("#conf_dom_tipoactivacion").html('');
        $("#conf_dom_ctaconsolidar").html('');
        $("#conf_dom_email").html('');
        $("#conf_dom_financiamiento").html('');
        $("#conf_dom_equipo").html('');
        $("#conf_dom_imei").html('');
        $("#conf_dom_concepto").html('');

        $("#observacionesadicionales2").val('');

        

        //para todos los input
        $('.has-success, .has-error').removeClass('has-success').removeClass('has-error');
        $('.select2-has-success, .select2-has-error').removeClass('select2-has-success').removeClass('select2-has-error');

        //reseteo de boton de envio
        $('#botonguardardomicilio').prop('disabled', true);
        
    }

    //SE RESETEA TODO AL CLICKEAR EL BOTON DE RESETEO
    $('#resetall_domicilio').click(function(){
        resetfields();
        resetfields2();
    });

    //
    $('#capturadomiciliomodalbtn').click(function(){
        $('input').trigger('click').trigger('change').trigger('keyup');
        $('#body-form2').find('select').trigger('change');
        $('#btn_domsearchdata').trigger('click');
        var mandatory = $('.mandatory2').length;
        var mandatorywithhassuccess = $('.has-success').find('.mandatory2').length;
        var simultaneusselector = $('.mandatory2.has-success').length;
        var total_validated = mandatorywithhassuccess + simultaneusselector;

        var witherror = $('.has-error').length;

        if (mandatory == total_validated) {
            $('#botonguardardomicilio').prop('disabled', false);
        } else {
            $('#botonguardardomicilio').prop('disabled', true);
        }

        console.log(mandatory)
        console.log(total_validated)
        
    });
    

   
    //BUSQUEDA DE CAPTURAS EN TABLAS AL HACER KEYUP
    $('#search_capturanormal').keyup(function () {
        var filterBy = this;
        $.each($('.rows_capturas'), function (i, val) {
            if ($(val).text().toLowerCase().indexOf($(filterBy).val().toLowerCase()) == -1) {
                $('.rows_capturas').eq(i).hide();
            } else {
                $('.rows_capturas').eq(i).show();
            }
        });
    });

    $('#search_capturacorreccion').keyup(function () {
        var filterBy = this;
        $.each($('.rows_correcciones'), function (i, val) {
            if ($(val).text().toLowerCase().indexOf($(filterBy).val().toLowerCase()) == -1) {
                $('.rows_correcciones').eq(i).hide();
            } else {
                $('.rows_correcciones').eq(i).show();
            }
        });
    });

    $('#search_capturadomicilio').keyup(function () {
        var filterBy = this;
        $.each($('.rows_domicilio'), function (i, val) {
            if ($(val).text().toLowerCase().indexOf($(filterBy).val().toLowerCase()) == -1) {
                $('.rows_domicilio').eq(i).hide();
            } else {
                $('.rows_domicilio').eq(i).show();
            }
        });
    });

    
    //$('#botonguardardomicilio').prop('disabled', false);


});