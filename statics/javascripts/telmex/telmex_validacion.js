$(document).ready(function () {
  //EVITA EL SUBMIT AL CLICKEAR ANTER
  $(window).keydown(function (event) {
    if (event.keyCode == 13) {
      event.preventDefault();
      return false;
    }
  });

  //select2
  $(
    `#selectactivacion_tipodecaptura,
    #selectactivacion_tipodeidentificacion,
    #selectactivacion_tiposervicio_,
     #selectactivacion_paqueteinfinitum,
     #selectactivacion_conentretenimiento,
     #selectactivacion_cierre,

    #selectentidadfederativa,
    #selectestatuslinea,
    #selecttipocliente,
    #selecttiposervicio,
    #selecttiposervicioresidencial
    `
  ).select2({
    placeholder: "Selecciona una opción",
    allowClear: false,
    width: "100%",
  });

  //validacion activacion
  /*    #selectactivacion_tipodecaptura
        #activacion_nombres
        #activacion_paterno
        #activacion_materno
        #activacion_nombrecomercial
        #activacion_nombredefacturacion
        #activacion_rfc
        #selectactivacion_tipodeidentificacion
        #activacion_numerodeidentificacion
        #activacion_celular
        #activacion_telefonoadicional
        #activacion_correoelectronico
        #activacion_estado
        #activacion_municipio
        #activacion_colonia
        #activacion_cp
        #activacion_calle
        #activacion_numeroexterior
        #activacion_numerointerior
        #activacion_manzana
        #activacion_lote
        #activacion_edificio
        #activacion_departamento
        #activacion_calificador
        #activacion_subnumero
        #activacion_entrecalleuno
        #activacion_entrecalledos
        #selectactivacion_tiposervicio_
        #selectactivacion_paqueteinfinitum
        #selectactivacion_conentretenimiento
        #selectactivacion_cierre
        #activacion_siac
        #guardar_activacion */

  //VALIDACION DE LOS SELECTS

  $(
    `#selectactivacion_tipodecaptura,
    #selectactivacion_tipodeidentificacion,
    #selectactivacion_tiposervicio_,
     #selectactivacion_paqueteinfinitum,
     #selectactivacion_conentretenimiento,
     #selectactivacion_cierre`
  ).change(function () {
    var selector = $(this)
      .next()
      .find(".select2-selection.select2-selection--single");
    var value = $(this).val();
    if (value == "") {
      $(this).closest("div").removeClass("has-success");
      $(this).closest("div").addClass("has-error");
      selector.removeClass("select2-has-success");
      selector.addClass("select2-has-error");
    } else {
      $(this).closest("div").removeClass("has-error");
      $(this).closest("div").addClass("has-success");
      selector.removeClass("select2-has-error");
      selector.addClass("select2-has-success");
    }
    resetcoloremptyfields(selector, value);
  });

  //VALIDACION DE INPUTS
  //   #activacion_nombres
  // #activacion_paterno
  // #activacion_materno
  // #activacion_nombrecomercial
  // #activacion_nombredefacturacion
  // #activacion_rfc
  // #activacion_numerodeidentificacion
  // #activacion_celular
  // #activacion_telefonoadicional
  // #activacion_correoelectronico
  // #activacion_estado
  // #activacion_municipio
  // #activacion_colonia
  // #activacion_cp
  // #activacion_calle
  // #activacion_numeroexterior
  // #activacion_numerointerior
  // #activacion_manzana
  // #activacion_lote
  // #activacion_edificio
  // #activacion_departamento
  // #activacion_calificador
  // #activacion_subnumero
  // #activacion_entrecalleuno
  // #activacion_entrecalledos
  // #activacion_siac

  $("#activacion_nombres").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_paterno").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_materno").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_nombrecomercial").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_nombredefacturacion").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_rfc").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\w{10,13}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_numerodeidentificacion").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{13}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_celular").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d{10}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_telefonoadicional").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d{10}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_correoelectronico").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[\w.\-\_ñ]+@\w+(\.\w+)+$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_estado").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_municipio").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_colonia").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_cp").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d+$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_calle").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_numeroexterior").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d+$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_numerointerior").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d+$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_manzana").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_lote").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_edificio").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_departamento").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_calificador").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_subnumero").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_entrecalleuno").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_entrecalledos").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#activacion_siac").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,8}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });

  //---------------------------------------------------------
  //---------------------------------------------------------
  //---------------------------------------------------------
  //---------------------------------------------------------
  //---------------------------------------------------------

  /*
#nombres
#paterno
#materno
#celular
#fijo
#email
#horariovisitalaboral
#numeroaportar
#calle
#lote
#cruzamientos
#colonia
#codigopostal
#ciudad
#estado
#guardar_portabilidad


#selectentidadfederativa
#selectestatuslinea
#selecttipocliente
#selecttiposervicio
#selecttiposervicioresidencial
*/

  //VALIDACION DE LOS SELECTS

  $(
    `
    #selectentidadfederativa,
#selectestatuslinea,
#selecttipocliente,
#selecttiposervicio,
#selecttiposervicioresidencial
    `
  ).change(function () {
    var selector = $(this)
      .next()
      .find(".select2-selection.select2-selection--single");
    var value = $(this).val();
    if (value == "") {
      $(this).closest("div").removeClass("has-success");
      $(this).closest("div").addClass("has-error");
      selector.removeClass("select2-has-success");
      selector.addClass("select2-has-error");
    } else {
      $(this).closest("div").removeClass("has-error");
      $(this).closest("div").addClass("has-success");
      selector.removeClass("select2-has-error");
      selector.addClass("select2-has-success");
    }
    resetcoloremptyfields(selector, value);
  });

  //-campos estandar --------------------------------------------

  $("#nombres").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#paterno").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#materno").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#celular").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d{10}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#fijo").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d{7,10}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#email").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[\w.\-\_ñ]+@\w+(\.\w+)+$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#horariovisitalaboral").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#numeroaportar").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d{7,10}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#calle").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#lote").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#cruzamientos").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#colonia").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#codigopostal").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^\d+$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#ciudad").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });
  $("#estado").on("keyup", function () {
    var selector = $(this);
    var valorselector = $(this).val();
    var regex = /^[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
    colorvalidacion(selector, valorselector, regex);
    resetcoloremptyfields(selector, valorselector);
  });

  //----------------------------------------------
  //----------------------------------------------
  //---------------GLOBALES----------------------
  //----------------------------------------------
  //----------------------------------------------

  //reset empty fields
  resetcoloremptyfields = function (selector, valorselector) {
    if (valorselector == "") {
      selector
        .closest("div")
        .removeClass("has-success")
        .removeClass("has-error");
      selector
        .removeClass("select2-has-success")
        .removeClass("select2-has-error");
    }
  };

  //Funcion para colorear
  colorvalidacion = function (selector, valorselector, regex) {
    if (valorselector.match(regex)) {
      selector.closest("div").removeClass("has-error");
      selector.closest("div").addClass("has-success").addClass("has-feedback");
    } else {
      selector.closest("div").removeClass("has-success");
      selector.closest("div").addClass("has-error").addClass("has-feedback");
    }
  };
});
