$(document).ready(function () {
  //------------ P O R T A B I L I D A D ----------------------
  //------------ P O R T A B I L I D A D ----------------------
  //------------ P O R T A B I L I D A D ----------------------
  //------------ P O R T A B I L I D A D ----------------------
  //------------ P O R T A B I L I D A D ----------------------
  //------------ P O R T A B I L I D A D ----------------------


    let enviar_portabilidad = function(){

        $.post('/guardar_portabilidad', {
            asesor: $('#asesorname').data('name'),
            nombres: $('#nombres').val(),
            paterno: $('#paterno').val(),
            materno: $('#materno').val(),
            celular: $('#celular').val(),
            fijo: $('#fijo').val(),
            email: $('#email').val(),
            horariovisitalaboral: $('#horariovisitalaboral').val(),
            numeroaportar: $('#numeroaportar').val(),
            calle: $('#calle').val(),
            lote: $('#lote').val(),
            cruzamientos: $('#cruzamientos').val(),
            colonia: $('#colonia').val(),
            codigopostal: $('#codigopostal').val(),
            ciudad: $('#ciudad').val(),
            estado: $('#estado').val(),
            entidadfederativa: $('#selectentidadfederativa').val(),
            estatuslinea: $('#selectestatuslinea').val(),
            tipocliente: $('#selecttipocliente').val(),
            tiposervicio: $('#selecttiposervicio').val(),
            tiposervicioresidencial: $('#selecttiposervicioresidencial').val(),
        }, function(response){
            if(response.status == 'success'){
                alert('Guardado correctamente')
                reset_telmex_inputs()
            }else{
                alert(response.mensaje)
            }
        });
       /*
        #nombres
        #paterno
        #materno
        #celular
        #fijo
        #email
        #horariovisitalaboral
        #numeroaportar
        #calle
        #lote
        #cruzamientos
        #colonia
        #codigopostal
        #ciudad
        #estado
        #selectentidadfederativa
        #selectestatuslinea
        #selecttipocliente
        #selecttiposervicio
        #selecttiposervicioresidencial
        */


    }

  $("#guardar_portabilidad").click(function () {
    //para enviar y guardar
    var mandatory = $(".mandatory_portabilidad").length;
    var mandatorywithhassuccess = $(".has-success").find(
      ".mandatory_portabilidad"
    ).length;
    var simultaneusselector = $(".mandatory_portabilidad.has-success").length;
    var total_validated = mandatorywithhassuccess + simultaneusselector;

    var witherror = $(".has-error").length;

    if (mandatory == total_validated && witherror == 0) {
      //tomar los datos y enviarlo
      console.log(mandatory);
      console.log(mandatorywithhassuccess);
      console.log(simultaneusselector);
      enviar_portabilidad();
    } else {
      //marcar error
      alert(
        "Llenar todos los campos obligatorios marcados con asterisco/Validar errores."
      );
    }
  });

  //------------ A C T I V A C I O N E S ----------------------
  //------------ A C T I V A C I O N E S ----------------------
  //------------ A C T I V A C I O N E S ----------------------
  //------------ A C T I V A C I O N E S ----------------------
  //------------ A C T I V A C I O N E S ----------------------
  //------------ A C T I V A C I O N E S ----------------------

  let enviar_activacion = function(){
    $.post('/guardar_activacion', {
        asesor: $('#asesorname').data('name'),
        selectactivacion_tipodecaptura: $('#selectactivacion_tipodecaptura').val(),
        activacion_nombres: $('#activacion_nombres').val(),
        activacion_paterno: $('#activacion_paterno').val(),
        activacion_materno: $('#activacion_materno').val(),
        activacion_nombrecomercial: $('#activacion_nombrecomercial').val(),
        activacion_nombredefacturacion: $('#activacion_nombredefacturacion').val(),
        activacion_rfc: $('#activacion_rfc').val(),
        selectactivacion_tipodeidentificacion: $('#selectactivacion_tipodeidentificacion').val(),
        activacion_numerodeidentificacion: $('#activacion_numerodeidentificacion').val(),
        activacion_celular: $('#activacion_celular').val(),
        activacion_telefonoadicional: $('#activacion_telefonoadicional').val(),
        activacion_correoelectronico: $('#activacion_correoelectronico').val(),
        activacion_estado: $('#activacion_estado').val(),
        activacion_municipio: $('#activacion_municipio').val(),
        activacion_colonia: $('#activacion_colonia').val(),
        activacion_cp: $('#activacion_cp').val(),
        activacion_calle: $('#activacion_calle').val(),
        activacion_numeroexterior: $('#activacion_numeroexterior').val(),
        activacion_numerointerior: $('#activacion_numerointerior').val(),
        activacion_manzana: $('#activacion_manzana').val(),
        activacion_lote: $('#activacion_lote').val(),
        activacion_edificio: $('#activacion_edificio').val(),
        activacion_departamento: $('#activacion_departamento').val(),
        activacion_calificador: $('#activacion_calificador').val(),
        activacion_subnumero: $('#activacion_subnumero').val(),
        activacion_entrecalleuno: $('#activacion_entrecalleuno').val(),
        activacion_entrecalledos: $('#activacion_entrecalledos').val(),
        selectactivacion_tiposervicio_: $('#selectactivacion_tiposervicio_').val(),
        selectactivacion_paqueteinfinitum: $('#selectactivacion_paqueteinfinitum').val(),
        selectactivacion_conentretenimiento: $('#selectactivacion_conentretenimiento').val(),
        selectactivacion_cierre: $('#selectactivacion_cierre').val(),
        activacion_siac: $('#activacion_siac').val(),
    }, function(response){
        if(response.status == 'success'){
            alert('Guardado correctamente')
            reset_telmex_inputs()
        }else{
            alert(response.mensaje)
        }
    });
  /*    #selectactivacion_tipodecaptura
        #activacion_nombres
        #activacion_paterno
        #activacion_materno
        #activacion_nombrecomercial
        #activacion_nombredefacturacion
        #activacion_rfc
        #selectactivacion_tipodeidentificacion
        #activacion_numerodeidentificacion
        #activacion_celular
        #activacion_telefonoadicional
        #activacion_correoelectronico
        #activacion_estado
        #activacion_municipio
        #activacion_colonia
        #activacion_cp
        #activacion_calle
        #activacion_numeroexterior
        #activacion_numerointerior
        #activacion_manzana
        #activacion_lote
        #activacion_edificio
        #activacion_departamento
        #activacion_calificador
        #activacion_subnumero
        #activacion_entrecalleuno
        #activacion_entrecalledos
        #selectactivacion_tiposervicio_
        #selectactivacion_paqueteinfinitum
        #selectactivacion_conentretenimiento
        #selectactivacion_cierre
        #activacion_siac
        */

  }

  $("#guardar_activacion").click(function () {
    //para enviar y guardar
    var mandatory = $(".mandatory_activacion").length;
    var mandatory_activacion_camposderesidencial = $(".mandatory_activacion_camposderesidencial").length;
    var mandatory_activacion_camposdecomercial = $(".mandatory_activacion_camposdecomercial").length;

    var mandatory_activacion_paqueteinfinitum = $(".mandatory_activacion_paqueteinfinitum").length;
    var mandatory_activacion_conentretenimiento = $(".mandatory_activacion_conentretenimiento").length;

    var mandatory_activacion_camposiac = $(".mandatory_activacion_camposiac").length;
    
    //validar los valores de los campos condicionantes
    let tipo_activacion = $('#selectactivacion_tipodecaptura').val();
    let tipo_servicio = $('#selectactivacion_tiposervicio_').val();
    let tipo_cierre = $('#selectactivacion_cierre').val();

    //validacion tipo de activacion
    var validado_tipo_activacion = false;
    
    switch(tipo_activacion){
        case 'Residencial':
            var mandatorywithhassuccess = $(".has-success").find(".mandatory_activacion_camposderesidencial").length;
            if(mandatorywithhassuccess == mandatory_activacion_camposderesidencial){
                validado_tipo_activacion = true
            }else{
                validado_tipo_activacion = false
                console.log(mandatorywithhassuccess +'---'+ mandatory_activacion_camposderesidencial)
            }
            break;
        case 'Comercial':
             var mandatorywithhassuccess = $(".has-success").find(".mandatory_activacion_camposdecomercial").length;
            if(mandatorywithhassuccess == mandatory_activacion_camposdecomercial){
                validado_tipo_activacion = true
            }else{
                validado_tipo_activacion = false
                console.log(mandatorywithhassuccess +'---'+ mandatory_activacion_camposdecomercial)
            }
            break;
    }
    //validacion tipo de servicio
    var validado_tipo_servicio = false;
    switch(tipo_servicio){
        case 'Paquete infinitum':
            var mandatorywithhassuccess = $(".has-success").find(".mandatory_activacion_paqueteinfinitum").length;
            if(mandatorywithhassuccess == mandatory_activacion_paqueteinfinitum){
                validado_tipo_servicio = true
            }else{
                validado_tipo_servicio = false
            }
            break;
        case 'Con entretenimiento':
            var mandatorywithhassuccess = $(".has-success").find(".mandatory_activacion_conentretenimiento").length;
            if(mandatorywithhassuccess == mandatory_activacion_conentretenimiento){
                validado_tipo_servicio = true
            }else{
                validado_tipo_servicio = false
            }
            break;
    }
    //validacion tipo de cierre
    var validado_tipo_cierre = false;
    switch(tipo_cierre){
        case 'Captura exitosa':
            var mandatorywithhassuccess = $(".has-success").find(".mandatory_activacion_camposiac").length;
            if(mandatorywithhassuccess == mandatory_activacion_camposiac){
                validado_tipo_cierre = true
            }else{
                validado_tipo_cierre = false
            }
            break;
        case 'Sin cobertura':
            validado_tipo_cierre = true
            break;
    }


    //validacion de los campos generales
    var mandatorywithhassuccess = $(".has-success").find(".mandatory_activacion").length;
    var simultaneusselector = $(".mandatory_activacion.has-success").length;
    var total_validated = mandatorywithhassuccess + simultaneusselector;

    var witherror = $(".has-error").length;

    if (mandatory == total_validated && witherror == 0 && validado_tipo_activacion && validado_tipo_servicio && validado_tipo_cierre) {
      //tomar los datos y enviarlo
      enviar_activacion()
    } else {
      //marcar error
      console.log(mandatory)
      console.log(total_validated)
      console.log(validado_tipo_activacion)
      console.log(validado_tipo_servicio)
      console.log(validado_tipo_cierre)
      alert("Llenar todos los campos obligatorios marcados con asterisco/Validar errores.");
    }
  });
});
