var reset_telmex_inputs;
$(document).ready(function () {
  //RESET GENERAL DE CAMPOS

  reset_telmex_inputs = function () {
    $("input, select")
      .val("")
      .trigger("change")
      .closest("div")
      .removeClass("has-success")
      .removeClass("has-error")
      .removeClass("select2-has-success")
      .removeClass("select2-has-error");
  };

  //botones para mostrar formularios --------------------------
  $("#nueva_activacion").click(function () {
    $(".hiddablefield").hide();
    $(".container_activacion").show();
    reset_telmex_inputs();
  });

  $("#nueva_portabilidad").click(function () {
    $(".hiddablefield").hide();
    $(".container_portabilidad").show();
    reset_telmex_inputs();
  });

  //condicionales de portabilidad --------------------------------

  //condicionales de activacion ----------------------------------
  //tipo de captura residencia o comercial
  $("#selectactivacion_tipodecaptura").change(function () {
    let seleccion = $(this).val();
    switch (seleccion) {
      case "Residencial":
        $(".camposdecomercial").hide();
        $(".camposderesidencial").show();
        $(".camposdecomercial").find("input").val("").trigger("keyup");
        break;
      case "Comercial":
        $(".camposderesidencial").hide();
        $(".camposdecomercial").show();
        $(".camposderesidencial").find("input").val("").trigger("keyup");
        break;
    }
  });

  //tipo de servicio vendido
  $("#selectactivacion_tiposervicio_").change(function () {
    let seleccion = $(this).val();
    switch (seleccion) {
      case "Paquete infinitum":
        $(".paqueteinfinitum").show();
        $(".conentretenimiento")
          .hide()
          .find("select")
          .val("")
          .trigger("change");
        break;
      case "Con entretenimiento":
        $(".paqueteinfinitum").hide().find("select").val("").trigger("change");
        $(".conentretenimiento").show();

        break;
    }
  });

  //cierre de captura
  $("#selectactivacion_cierre").change(function () {
    let seleccion = $(this).val();
    switch (seleccion) {
      case "Captura exitosa":
        $(".camposiac").show();
        break;
      case "Sin cobertura":
        $(".camposiac").hide().find("input").val("").trigger("keyup");

        break;
    }
  });
});
