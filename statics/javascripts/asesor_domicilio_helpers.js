/*
Este documento sirve para la interaccion en las tablas que muestran
los datos de los tipos de capturas
*/

$(document).ready(function(){

    $('#mdom_closebutton').click(function(){
        $('.container_modaldomicilio').css('display','none'); //ocultar modal de detalles
        $('body').css('overflow', 'auto'); //para habilitar el scroll nuevamente
        $('#overlay-back').css('display', 'none'); //ocultar overlay
    });

    //visualizacion dinamica de las tablas del perfil del asesor
    
    $('.btn_tabla_capturasnormales').click(function () {
        $('#correcciones').css('display', 'none');
        $('#tablacapturas').css('display', 'block');
        $('#tabladomicilio').css('display', 'none');
    });

    $('.btn_tabla_correcciones').click(function () {
        $('#correcciones').css('display', 'block');
        $('#tablacapturas').css('display', 'none');
        $('#tabladomicilio').css('display', 'none');
    });

    $('.btn_tabla_capturasdomicilio').click(function () {
        $('#correcciones').css('display', 'none');
        $('#tablacapturas').css('display', 'none');
        $('#tabladomicilio').css('display', 'block');
    });
    
    //eventos en las filas de la tablas de domicilio para mostrar modal de detalles segun 
    //el id de la captura

    $('#statusdomicilio').on('click', '.rows_domicilio', function () {
        var idshipping = $(this).data('idshipping');
        $('#mdom_id').html(idshipping);
        $.post('/getDetalleDomicilio',{idshipping:idshipping},function(response){

            for(i in response[0]){
                if (response[0][i] == null || response[0][i] == "Invalid date") {
                    response[0][i] = "N/A"
                }
            }
            
            
            $('#mdom_fecha').html(response[0]['created']);
            $('#mdom_status').html(response[0]['status']);

            $('#detalles_cliente').html(response[0]['clientname']);
            $('#detalles_contacto').html(response[0]['contact']);
            $('#detalles_numamigrar').html(response[0]['mainline']);
            $('#detalles_destino').html(response[0]['city']);
            $('#detalles_fechaentrega').html(response[0]['scheduled']);
            $('#detalles_enganche').html(response[0]['enganche']);
            $('#detalles_horaentrega').html(response[0]['time']);
            $('#detalles_direccion').html(response[0]['address']);
            $('#detalles_colonia').html(response[0]['postalcode']);
            $('#detalles_referencias').html(response[0]['reference']);

            $('#detalles_sisact').html(response[0]['sisact']);
            $('#detalles_canal').html(response[0]['channel']);
            $('#detalles_plan').html(response[0]['nameplan']);
            $('#detalles_clave').html(response[0]['keynameplan']);
            $('#detalles_plazo').html(response[0]['termplan']);
            $('#detalles_tipoplan').html(response[0]['typeplan']);
            $('#detalles_tipo').html(response[0]['type']);
            $('#detalles_ctaconsolidar').html(response[0]['account']);
            $('#detalles_email').html(response[0]['email']);
            $('#detalles_equipo').html(response[0]['phone']);
            $('#detalles_imei').html(response[0]['imei']);
            $('#detalles_financiemiento').html(response[0]['fineq']);
            $('#detalles_pagara').html(response[0]['concept']);
            $('#detalles_observaciones').html(response[0]['additionalcomment']);

            $('#detalles_factura').html(response[0]['invoice']);
            $('#detalles_iccid').html(response[0]['iccid']);
            $('#detalles_guia').html(response[0]['guidenumber']);
            $('#detalles_proveedor').html(response[0]['provider']);
            $('#detalles_fechaactivacion').html(response[0]['activation_date']);
            $('#detalles_fechaenvio').html(response[0]['datewithprovider']);
            $('#detalles_fechaentregado').html(response[0]['delivered_date']);

            $('#detalles_datestatus').html(response[0]['datestatus']);
            $('#detalles_comentario').html(response[0]['comment']);



        });

        $('.container_modaldomicilio').css('display', 'block'); //mostrar modal detalles
        $('body').css('overflow','hidden'); //deshabilitar scroll del bg
        $('#overlay-back').css('display', 'block'); //mostrar overlay


    });

});