var dailyandmonthly, statisticsanalista, statisticscerrador, toptenactivadores, countercampaign, shametopten, graficaHighchart, update_tableconnections, getDailyCreatedByCampaign, getCorrections, getReferidos, change_stat, cambiar_estatus
$(document).ready(function(){

    //-----------------------------------------------------------------------
    //-------------------------------WEBSOCKETS------------------------------
    //-----------------------------------------------------------------------

    socket = io.connect('http://'+location.host);
    console.log("websocket addr: http://"+location.host)

    
    socket.on('refreshdashboard', function (msg) {
        console.log('Actualizando divs');
        dailyandmonthly();
        statisticsanalista();
        statisticscerrador();
        toptenactivadores();
        countercampaign();
        shametopten();
        graficaHighchart();
        update_tableconnections();
        getDailyCreatedByCampaign();
        getCorrections();
        feed_whatsappcounter();
        getReferidos();
    });

    
    //AL HABER CAMBIOS EJECUTO LA FUNCION
    socket.on('update_tableconnections', function (msg) {
        setTimeout(update_tableconnections,2000);
    });




    

    //--------------------FUNCION QUE INVOCA EL ROUTE-------------------------
    //custom_routes.js
    update_tableconnections = function () {
        //var update = function(){
        $.post('/getNodeConnections_DISABLEDDELETETHIS', function (response) {

            $('#contadorconexionestabla').html(response.length);
            $('#contadorconexioneslinkdashboard').html(response.length);

            var table_body = [];
            for (i = 0; i < response.length; i++) {
                table_body += '<tr>';

                table_body += '<td>';
                table_body += "<img src='../assets/connected.png' alt='icon_conexion'><span> &nbsp;&nbsp;"
                table_body += response[i]["asesor"] + "  <span style='color:grey;'> (" + response[i]["campania"] + ")</span>";
                table_body += '</span></td>';

                if (response[i]["borrador"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["borrador"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["borrador"];
                    table_body += '</td>';
                }

                if (response[i]["enviado"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["enviado"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["enviado"];
                    table_body += '</td>';
                }

                if (response[i]["corregir"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["corregir"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["corregir"];
                    table_body += '</td>';
                }

                if (response[i]["enespera"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["enespera"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["enespera"];
                    table_body += '</td>';
                }

                if (response[i]["rechazada"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["rechazada"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["rechazada"];
                    table_body += '</td>';
                }

                if (response[i]["aceptada"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["aceptada"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["aceptada"];
                    table_body += '</td>';
                }

                if (response[i]["aceptadacc"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["aceptadacc"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["aceptadacc"];
                    table_body += '</td>';
                }

                if (response[i]["nofinalizada"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["nofinalizada"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["nofinalizada"];
                    table_body += '</td>';
                }

                if (response[i]["activa"] != 0) {
                    table_body += "<td style='color:orange'>";
                    table_body += response[i]["activa"];
                    table_body += '</td>';
                } else {
                    table_body += "<td>";
                    table_body += response[i]["activa"];
                    table_body += '</td>';
                }

                table_body += '</tr>';
            }
            $('#tbody_tableconnections').html(table_body);
        });
        //}

        //setTimeout(update,5000);

    }



    //evento que refresca manualmente la tabla de conexiones
    $('#updateConnectionsNow').on('click', function(){
        update_tableconnections();
    });

    

    //-----------------------------------------------------------------------
    //-------------------------------WEBSOCKETS------------------------------
    //-----------------------------------------------------------------------

    

    dailyandmonthly = function () {
        //custom_routes_supervisor
        $.post('/getDataLeftDashboard', function (response) {

            //lado derecho del dashboard en apartado de graficas
            $('#hccounter_capturas').html(response[0]["capturas"]);
            $('#hccounter_activaciones').html(response[0]["activaciones"]);
            
            //bigboxes principales
            $('#bc_capturas').html(response[0]["capturasdia"]);
            $('#bc_activaciones').html(response[0]["activacionesdia"]);
            $('#bc_rechazos').html(response[0]["rechazosdia"]);
        });
    } // fin de function

    statisticsanalista = function(){
        //custom_routes_analista
        $.post('/statisticsanalista', function (response) {
            $('#lrana_pendientes').html(response[0]["pendientes"]);
            $('#lrana_enespera').html(response[0]["enesperaderespuesta"]);
            $('#lrana_aceptadas').html(response[0]["aceptadas"]);
            $('#lrana_correcciones').html(response[0]["correcciones"]);
        });
    } // fin de function

    statisticscerrador = function () {
        //custom_routes_supervisor
        $.post('/statisticscerrador', function (response) {
            $('#lrcerr_concentrado').html(response[0]["concentrado"]);
            $('#lrcerr_aceptadas').html(response[0]["aceptadassin"]);
            $('#lrcerr_agendadas').html(response[0]["agendadas"]);
            $('#lrcerr_domicilio').html(response[0]["aceptadasdom"]);
        });
    }// fin de function

    toptenactivadores = function(){
        $.post('/feedactivadores', function (response) {
            var table_body = [];
            for (i = 0; i < response.length; i++) {
                table_body += '<tr>';

                table_body += '<td>';
                table_body += response[i]["person_name"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["quantity"];
                table_body += '</td>';

                table_body += '</tr>';
            }
            $('#tbody_topactivadores').html(table_body);
        });
    }//fin de function

    shametopten = function () {
        $.post('/shametopten', function (response) {
            var table_body = [];
            for (i = 0; i < response.length; i++) {
                table_body += '<tr>';

                table_body += '<td>';
                table_body += response[i]["vendedor"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["capturas"];
                table_body += '</td>';

                table_body += '</tr>';
            }
            $('#tbody_topbajaparticipacion').html(table_body);
        });
    }//fin de function

    countercampaign = function () {
        //custom_routes_supervisor
        $.post('/countercampaign', function (response) {
            $('#numbers_excelentes').html(response[0]["excelentes"]);
            $('#numbers_estrena').html(response[0]["estrena"]);
        });
    }// fin de function

    getDailyCreatedByCampaign = function () {
        //custom_routes_supervisor
        $.post('/getDailyCreatedByCampaign', function (response) {
            $('#cdiarias_excelentes').html(response[0]["excelentes"]);
            $('#cdiarias_estrena').html(response[0]["estrena"]);
        });
    }// fin de function
    
    graficaHighchart = function () {

        labels_activaciones = new Array(31);
        labels_capturas = [];
        labels_graph = [];
        
        var fecha = new Date();
        
        //los labels son dinamicos. Desde el dia 1 hasta el dia de hoy con .getDate()
        for(i = 0; i < fecha.getDate(); i ++){
            labels_graph.push((i+1));
        }
      
        
        $.post('/getActivasBlackdashboard', function (response) {
            

            /*for (i = 0; i < fecha.getDate(); i++) {
                for(j = 0; j <response.length; j++){
                    if(i==(response[j]['dia']-1)){
                        newvalue = Number(response[j]['activas']);
                        labels_activaciones[i] = newvalue;
                    }else{
                        labels_activaciones[i] = 0;
                    }
                }
        }*/

            for (i = 0; i < response.length; i++) {
                labels_activaciones[(response[i]["dia"]) - 1] = Number(response[i]["activas"]);
            }

            for (i = 0; i < fecha.getDate(); i++) {
                if (labels_activaciones[i] == null) {
                    labels_activaciones[i] = 0
                }
            }

            $.post('/getCapturasBlackdashboard', function (response) {
                for (i = 0; i < response.length; i++) {
                    labels_capturas[(response[i]["dia"]) - 1] = Number(response[i]["capturas"]);
                }

                for (i = 0; i < fecha.getDate(); i++) {
                    if (labels_capturas[i] == null) {
                        labels_capturas[i] = 0
                    }
                }

                Highcharts.chart('highchart_main', {
                    chart: {
                        type: 'spline',

                    },
                    title: {
                        text: 'Capturas y activas del mes'
                    },
                    subtitle: {
                        text: 'T E L E M A R K E T I N G'
                    },
                    xAxis: {
                        categories: labels_graph
                    },
                    yAxis: {
                        title: {
                            text: 'ACTIVACIONES'
                        }
                    },
                    plotOptions: {
                        series: {
                            animation: false
                        },
                        spline: {
                            marker: {
                                radius: 4,
                                lineColor: '#666666',
                                lineWidth: 1
                            }
                        }
                    },
                    series: [{
                        name: 'Activas',
                        data: labels_activaciones
                    }, {
                        name: 'Capturas',
                        data: labels_capturas
                    }]
                }); // FIN DEL HIGHCHART
            }); //FIN DEL SEGUNDO POST
        }); //FIN DEL PRIMER POST

    }

    getCorrections = function () {
        $.post('/analistacorrecciones', function (response) {
            var table_body = [];
            $('#contadorcorreccionestabla').html(response.length);
            $('#contadorcorreccioneslinkdashboard').html(response.length);
            for (i = 0; i < response.length; i++) {
                table_body += "<tr class='rows_correcciones' data-idrequest=' "+ response[i]["idregistro"] +"' >";

                table_body += '<td>';
                table_body += response[i]["fecha"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["cliente"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["vendedor"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["campania"];
                table_body += '</td>';

                if (response[i]["analistaasignado"] == null){
                    table_body += '<td>';
                    table_body += " "
                    table_body += '</td>';
                }else{
                    table_body += '<td>';
                    table_body += response[i]["analistaasignado"];
                    table_body += '</td>';
                }

                if (response[i]["corregido"] == 0){
                    table_body += '<td>';
                    table_body += "Esperando corrección";
                    table_body += '</td>';
                } else if (response[i]["corregido"] == 1){
                    table_body += '<td>';
                    table_body += "CORREGIDO";
                    table_body += '</td>';
                }

                

                

                table_body += '</tr>';
            }
            $('#tbody_tablecorrections').html(table_body);
        });
    }//fin de function

    //actualizar contadores de mensajes de whatsapp
    feed_whatsappcounter = () => {
        f = new Date();
        $.post('/counter_whatsapp',function(response){
            $('#wa_dia').html(response[0].conteo_dia);
            $('#wa_mes').html(response[0].conteo_mes);
            valor = (response[0].conteo_mes / f.getDate()) * 30;
            valor = valor.toFixed(2);
            $('#wa_proyeccion').html(valor);
        });
    }

    


//EXPORTAR REPORTE DE WHATSAPPS
    $('#descargarreportewa').click(function(){
        
        
        $.post('/exportar_wa',function(response){
            console.table(response);
            if (response == ''){
                console.log("Sin resultados");
                alert("Sin resultados");
                return;
            }else{
                JSONToCSVConvertor(response,"Whatsapps-ULTIMOS2MESES-"+response.length, true);
            }
        });

    });

//EXPORTAR ESTADISTICAS DEL MES ACTUAL DE LAS CAPTURAS DE REFERIDOS
    $('#btn_download_referidos').click(function(){
        
        
        $.post('/referidos_ultimomes',function(response){
            console.table(response);
            if (response == ''){
                console.log("Sin resultados");
                alert("Sin resultados");
                return;
            }else{
                JSONToCSVConvertor(response,"Referidos-ultimo-mes-"+response.length, true);
            }
        });

    });

///////////////////////////////////////////////////////////////
//funcion para exportar reportes en excel
//jsfiddle: http://jsfiddle.net/hybrid13i/JXrwM/
function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
    //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
    var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;

    var CSV = '';
    //Set Report title in first row or line

    CSV += ReportTitle + '\r\n\n';

    //This condition will generate the Label/Header
    if (ShowLabel) {
        var row = "";

        //This loop will extract the label from 1st index of on array
        for (var index in arrData[0]) {

            //Now convert each value to string and comma-seprated
            row += index + ',';
        }

        row = row.slice(0, -1);

        //append Label row with line break
        CSV += row + '\r\n';
    }

    //1st loop is to extract each row
    for (var i = 0; i < arrData.length; i++) {
        var row = "";

        //2nd loop will extract each column and convert it in string comma-seprated
        for (var index in arrData[i]) {
            row += '"' + arrData[i][index] + '",';
        }

        row.slice(0, row.length - 1);

        //add a line break after each row
        CSV += row + '\r\n';
    }

    if (CSV == '') {
        alert("Invalid data");
        return;
    }

    //Generate a file name
    var fileName = "Reporte_";
    //this will remove the blank-spaces from the title and replace it with an underscore
    fileName += ReportTitle.replace(/ /g, "_");

    //Initialize file format you want csv or xls
    var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

    // Now the little tricky part.
    // you can use either>> window.open(uri);
    // but this will not work in some browsers
    // or you will not get the correct file extension    

    //this trick will generate a temp <a /> tag
    var link = document.createElement("a");
    link.href = uri;

    //set the visibility hidden so it will not effect on your web-layout
    link.style = "visibility:hidden";
    link.download = fileName + ".csv";

    //this part will append the anchor tag and remove it after automatic click
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
}
////////////////////////////////////////////////////////////////

//REFERIDOS
    getReferidos = function(){
        $.post('/get_referidos',function(response){
            var table_body = [];
            $('#contadorreferidostabla').html(response.length);
            $('#contadorreferidoslinktabla').html(response.length);
            for (i = 0; i < response.length; i++) {
                table_body += "<tr class='rows_referidos' data-idrequest=' "+ response[i]["id"] +"' >";

                table_body += '<td>';
                table_body += moment(response[i]["creado"]).format("YYYY-MM-DD HH:mm:ss");
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["asesor"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["telefono"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["procedencia"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["tramite"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["observaciones"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["status"];
                table_body += '</td>';


                // A - acepta
                // S- seguimiento
                // R - Rechaza
                // jueves 21-05-2020
                // se agrega opcion de seguimiento En llamada
                table_body += '<td>';
                table_body += `<button onclick="change_stat(${response[i]["id"]})" style='color:#5DA4E2;width:100%'>Nuevo estatus</button>`
                table_body += `
                                <ul class="hide_remain_options" id="options_codif${response[i]["id"]}">
                                    <li onclick="cambiar_estatus('S',${response[i].id},'EN LLAMADA')">EN LLAMADA</li>
                                    <li onclick="cambiar_estatus('A',${response[i].id},'ACEPTA PROMOCIÓN')">ACEPTA PROMOCIÓN</li>
                                    <li onclick="cambiar_estatus('A',${response[i].id},'INTERESADO EN SEGUIMIENTO')">INTERESADO EN SEGUIMIENTO</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'BUZON')">BUZON</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'LISTA NEGRA')">LISTA NEGRA</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'CLIENTE REPROGRAMA')">CLIENTE REPROGRAMA</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'CORTA LLAMADA')">CORTA LLAMADA</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'FUERA DEL ÁREA DE SERVICIO')">FUERA DEL ÁREA DE SERVICIO</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'NO CONTACTADO / NO CONTESTA')">NO CONTACTADO / NO CONTESTA</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'NO INDICA')">NO INDICA</li>
                                    <li onclick="cambiar_estatus('S',${response[i].id},'SIN TIEMPO / OCUPADO')">SIN TIEMPO / OCUPADO</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'BAJOS CONSUMOS')">BAJOS CONSUMOS</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'CLIENTE CON ADEUDOS')">CLIENTE CON ADEUDOS</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'EQUIPO NO LE INTERESA / ALTO COSTO')">EQUIPO NO LE INTERESA / ALTO COSTO</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'NO DESEA DEJAR RENTAS ANTICIPADAS')">NO DESEA DEJAR RENTAS ANTICIPADAS</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'NO DESEA LLAMADA')">NO DESEA LLAMADA</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'NO DESEA MAS LINEAS')">NO DESEA MAS LINEAS</li>
                                    <li onclick="cambiar_estatus('R',${response[i].id},'PREFIERE OFERTA PREPAGO')">PREFIERE OFERTA PREPAGO</li>
                                </ul>    
                                `
                table_body += '</td>';
                
                table_body += '</tr>';
            }
            $('#tbody_tablereferidos').html(table_body);
        });
    }


    // change_stat = function(id){
    //     $.post('/discard_refered',{id:id},function(response){
    //         if(response == "ok"){
    //             getReferidos();
    //         }else{
    //             alert("Error: "+ response);
    //         }
    //     });
    // }

    cambiar_estatus = function(flag,id,estatus){
        console.log(id)
        console.log(estatus)
        console.log(flag)
        $.post('/cambio_estatus_referido',{id:id,estatus:estatus,flag:flag},function(response){
            if(response == "ok"){
                getReferidos();
            }else{
                alert("Error: "+ response);
            }
        });
    }

    change_stat = function(id){
        //$('.hide_remain_options').hide();
        $(`#options_codif${id}`).toggle(100);

    }




    //instancias iniciales
    dailyandmonthly();
    statisticsanalista();
    statisticscerrador();
    toptenactivadores();
    countercampaign();
    shametopten();
    getDailyCreatedByCampaign();
    graficaHighchart();
    getCorrections();
    feed_whatsappcounter();
    getReferidos();
    
});