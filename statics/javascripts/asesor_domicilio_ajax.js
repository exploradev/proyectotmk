var filltable;
$(document).ready(function(){
    
    filltable = function(){
        var asesor = $('#seccionconfirmarcaptura').data('user');  
        $.post('/getPrevisualizacionDomicilio',{asesor: asesor},function(response){
            var table_body = [];
            var cantidad = response.length;
            $('.cantidaddomicilio').html(cantidad);
            for (i = 0; i < response.length; i++) {
                table_body += "<tr id='cdomicilio" + response[i]['idshipping'] + "' class='rows_domicilio' data-idshipping='" + response[i]['idshipping'] + "'>";

                table_body += '<td>';
                table_body += response[i]["capturada"];
                table_body += '</td>';

                table_body += '<td>';
                if (response[i]["guia"] == null){
                    table_body += "Sin guía";
                }else{
                    table_body += response[i]["guia"];
                }
                table_body += '</td>';

                table_body += '<td>';
                if (response[i]["fechadeenviodelequipo"] == "Invalid date"){
                    table_body += "Aún sin enviar"
                }else{
                    table_body += response[i]["fechadeenviodelequipo"];
                }
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["cliente"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["numerodecontacto"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["estatus"];
                table_body += '</td>';

                table_body += '</tr>';
            }
            $('#statusdomicilio').html(table_body);
        });
    }


    filltable();
});