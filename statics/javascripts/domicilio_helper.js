
var styleThisTab;
$(document).ready(function(){

   

   //al estar el focus en el modal lo cierro con esc
   $(window).keyup(function (e) {
      if (e.keyCode === 27) $('#domicilio_closebutton').trigger('click');
      if (e.keyCode === 27) $('#domicilio_closebuttonexport').trigger('click');   // esc
   });

   //INICIALIZO EL CLIPBOARD Y EMULO CLICK AL HACER FOCUS A CADA CAMPO
   new ClipboardJS('.copytoclipboard');
   $('.copytoclipboard').focus(function(){
      var selector = $(this).data('clipboard-target');
      $(selector).trigger('click');
   });

   //INICIALIZO DATETIMEPICKER
   $('.datetimepicker_notime').datetimepicker({
      timepicker: false,
      format: 'Y/m/d',
      validateOnBlur: false,
      theme: 'dark'
   });

   //EVENTO PARA ESCRIBIR FECHA EN CAMPO DE ENVIADO
   $('#mdom_status').change(function (e) { 
      e.preventDefault();
      var valor = $(this).val();
      if(valor == 'Guia enviada'){
         var nvo_timestamp = Date.now();
         var timestampp = moment(nvo_timestamp).format('YYYY[/]MM[/]DD');
         $('#detalles_fechaenvio').val(timestampp);
      }

   });

   //para rangos en modal de exportacion

   jQuery('.date_timepicker_start').datetimepicker({
      format: 'Y-m-d',
      onShow: function (ct) {
         this.setOptions({
            maxDate: jQuery('.date_timepicker_end').val() ? jQuery('.date_timepicker_end').val() : false
         })
      },
      timepicker: false,
      theme: 'dark'
   });
   jQuery('.date_timepicker_end').datetimepicker({
      format: 'Y-m-d',
      onShow: function (ct) {
         this.setOptions({
            minDate: jQuery('.date_timepicker_start').val() ? jQuery('.date_timepicker_start').val() : false
         })
      },
      timepicker: false,
      theme: 'dark'
   });

   //CAMBIO LOS ESTILOS SEGUN EL TAB SELECCIONADO DE LOS FILTROS 
   //POR STATUS
   styleThisTab = function(thisTab){
    $('.buttonwithdata').removeClass('linktabactive');
    $('#' + thisTab).addClass('linktabactive');
   }

   //INICIALIZO LOS SELECT2 
   //ESTATUS
   $('#mdom_status').select2({
      dropdownParent: $(".modaldeseguimiento_container"),
      placeholder: "Selecciona una opcion",
      allowClear: false,
      //dropdownCssClass: 'selectplan'
   });

   //TIPO DE CAPTURA
   $('#detalles_tipo').select2({
      dropdownParent: $(".modaldeseguimiento_container"),
      placeholder: "Selecciona una opcion",
      allowClear: false,
      //dropdownCssClass: 'selectplan'
   });

   //PLANES
   $('#detalles_plan').select2({
      dropdownParent: $(".modaldeseguimiento_container"),
      placeholder: "Selecciona una opcion",
      allowClear: false,
      //dropdownCssClass: 'selectplan'
   });

   $('#detalles_formapago').select2({
      dropdownParent: $(".modaldeseguimiento_container"),
      placeholder: "Selecciona una opcion",
      allowClear: false,
      //dropdownCssClass: 'selectplan'
   });



   //AL HACER CLICK AL BOTON DE CERRAR MODAL
   $('#domicilio_closebutton').click(function () {
      $('.modaldeseguimiento_container').css('display', 'none');
      
      $('#overlay-back').css('display', 'none'); //ocultar overlay
   });

   $('#domicilio_closebuttonexport').click(function () {
      
      $('.modaldeexportacion_container').css('display', 'none');
      $('#overlay-back').css('display', 'none'); //ocultar overlay
   });


   //LLAMO A LA RUTA PARA ESCRIBIR DETALLES EN EL MODAL
   $('#tbody_domicilio').on('click','.rowhover',function () {
      
      //funcion alojada en domicilio_ajax.js
      
      $('#observacionstatus').val('');
      $('.modaldeseguimiento_container').css('display', 'block');

      var idshipping = $(this).data('idshipping');
      $('#mdom_id').html(idshipping);
      $.post('/getDetalleDomicilio', { idshipping: idshipping }, function (response) {

         for (i in response[0]) {
            if (response[0][i] == null || response[0][i] == "Invalid date") {
               response[0][i] = ""
            }
         }

         $('#asesorname').html(response[0]['asesor']);

         $('#mdom_fecha').html(response[0]['created']);
         $('#mdom_status').val(response[0]['status']).trigger('change');

         $('#detalles_cliente').val(response[0]['clientname']);
         $('#detalles_contacto').val(response[0]['contact']);
         $('#detalles_numamigrar').val(response[0]['mainline']);
         $('#detalles_destino').val(response[0]['city']);
         $('#detalles_fechaentrega').val(response[0]['scheduled']);
         $('#detalles_horaentrega').val(response[0]['time']);
         $('#detalles_direccion').val(response[0]['address']);
         $('#detalles_colonia').val(response[0]['postalcode']);
         $('#detalles_referencias').val(response[0]['reference']);
         $('#detalles_enganche').val(response[0]['enganche']);

         $('#detalles_sisact').val(response[0]['sisact']);
         $('#detalles_canal').val(response[0]['channel']);
         $('#detalles_plan').val(response[0]['idplan']).trigger('change');
         $('#detalles_clave').val(response[0]['keynameplan']);
         $('#detalles_plazo').val(response[0]['termplan']);
         $('#detalles_tipoplan').val(response[0]['typeplan']);
         $('#detalles_tipo').val(response[0]['type']).trigger('change');
         $('#detalles_ctaconsolidar').val(response[0]['account']);
         $('#detalles_email').val(response[0]['email']);
         $('#detalles_equipo').val(response[0]['phone']);
         $('#detalles_imei').val(response[0]['imei']);
         $('#detalles_financiemiento').val(response[0]['fineq']);
         $('#detalles_pagara').val(response[0]['concept']);
         $('#detalles_observaciones').val(response[0]['additionalcomment']);
         $('#detalles_formapago').val(response[0]['formapago']).trigger('change');

         $('#detalles_factura').val(response[0]['invoice']);
         $('#detalles_iccid').val(response[0]['iccid']);
         $('#detalles_guia').val(response[0]['guidenumber']);
         $('#detalles_proveedor').val(response[0]['provider']);
         $('#detalles_fechaactivacion').val(response[0]['activation_date']);
         $('#detalles_fechaenvio').val(response[0]['datewithprovider']);
         $('#detalles_fechaentregado').val(response[0]['delivered_date']);

         $('#date_comnt').html(response[0]['datestatus']);
         $('#comnt_comnt').html(response[0]['comment']);



      });

      
      
      $('#overlay-back').css('display', 'block'); //mostrar overlay
      
   });

   //-------------------------------------------------------------------
   //MODAL DE EXPORTACION

   $('#btn_exportaexcel').click(function(){
      $('.modaldeexportacion_container').css('display','block');
      $('#overlay-back').css('display', 'block'); //mostrar overlay
   });

   


});