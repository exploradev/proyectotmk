$(document).ready(function(){

    //get all fields
    //datos de la entrega

    $("input[name='dom_cliente']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^([\sa-zA-ZñÑ]*){1,100}$/;
        $('#conf_dom_cliente').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_fechaentrega']").on('change', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        //var regex = /[\d/]+/;
        var regex = /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/;
        $('#conf_dom_fechaentrega').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    $("input[name='dom_fechaentrega']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        //var regex = /[\d/]+/;
        var regex = /^\d{4}[\-\/\s]?((((0[13578])|(1[02]))[\-\/\s]?(([0-2][0-9])|(3[01])))|(((0[469])|(11))[\-\/\s]?(([0-2][0-9])|(30)))|(02[\-\/\s]?[0-2][0-9]))$/;
        $('#conf_dom_fechaentrega').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_horarioentrega']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^[:a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,30}$/;
        $('#conf_dom_horarioentrega').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_contacto']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d{10}$/;
        $('#conf_dom_contacto').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_numamigrar']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d{10}$/;
        $('#conf_dom_numamigrar').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_destino']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s]{1,30}$/;
        $('#conf_dom_destino').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_direccion']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,150}$/;
        $('#conf_dom_direccion').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_colonia']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,50}$/;
        $('#conf_dom_colonia').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_referencias']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^[[a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d]{1,300}$/;
        $('#conf_dom_referencias').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //---------------------------------------------------
    //---------------------------------------------------
    //---------------------------------------------------
    //---------------------------------------------------
    //---------------------------------------------------
    //---------------------------------------------------
    //---------------------------------------------------
    //------------------DATOS DEL TRAMITE----------------
    //---------------------------------------------------

    $("input[name='dom_folio']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d{1,10}$/;
        $('#conf_dom_folio').html(valorselector);

        //capturo el canal en caso de existir (se traslada al evento click)
        //var canal = $('#dom_canal').html();
        //$('#conf_dom_canal').html(canal);

        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    $("input[name='dom_canal']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\w{1,15}$/;
        $('#conf_dom_folio').html(valorselector);

        //capturo el canal en caso de existir (se traslada al evento click)
        //var canal = $('#dom_canal').html();
        //$('#conf_dom_canal').html(canal);

        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });



    $('#dom_plan').on('change', function () {
        var selector = $('.select2-selection.select2-selection--single');
        var nombredeplan = $(this).val();
        if (nombredeplan == '') {

            $(this).closest('div').removeClass('has-success');
            $(this).closest('div').addClass('has-error');
            $('.select2-selection.select2-selection--single').removeClass('select2-has-success');
            $('.select2-selection.select2-selection--single').addClass('select2-has-error');
        } else {

            $(this).closest('div').removeClass('has-error');
            $(this).closest('div').addClass('has-success');
            $('.select2-selection.select2-selection--single').removeClass('select2-has-error');
            $('.select2-selection.select2-selection--single').addClass('select2-has-success');
        }
        resetcoloremptyfields(selector, nombredeplan);

        //Aqui va para extraer la informacion e ingresarla  a la pestaña de confirmacion de captura

        var nombreplan = $(this).find(":selected").text();
        var claveplan = $(this).find(":selected").data("keyname");
        var plazoplan = $(this).find(":selected").data("term");
        
        //configurar info para pestaña de confirmacion
        $('#conf_dom_plan').html(nombreplan);
        $('#conf_dom_claveplan').html(claveplan);
        $('#conf_dom_plazoplan').html(plazoplan);


        if (claveplan == undefined) {
            $('#clavedeplan2').html('CLAVE:');
            $('#plazodeplan2').html('PLAZO:');
            $('#conf_claveplan').html("");
            $('#conf_plan').html("");
        } else {
            $('#clavedeplan2').html('CLAVE: ' + claveplan);
            $('#plazodeplan2').html('PLAZO: ' + plazoplan);
        }  

    });

    //-----------------------------------------------------

    $("select[name='dom_tipoactivacion']").on('change', function () {
        var selector = $(this);
        var valorselector = selector.val();
        var opciones = ["lineanueva","msr","migracion"];
        if($.inArray(valorselector,opciones)!= -1){
            selector.closest(".form-group").addClass("has-success");
        }else{
            //selector.closest(".form-group").addClass("has-error");
        }

        switch(valorselector){
            case "lineanueva":
                console.log("linea nueva");
                break;
            case "msr":
                console.log("msr");
                break;
            case "migracion":
                console.log("migracion");
                break;
            default:
                console.log('No se proporciono valor valido para el select');
        }
    });

    //-----------------------------------------------------

    $("input[name='dom_ctaconsolidar']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d{8}$/;
        $('#conf_dom_ctaconsolidar').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_email']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^[\w.\-\_ñ]+@\w+(\.\w+)+$/;
        $('#conf_dom_email').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_financiamiento']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d+\.\d+$/;
        $('#conf_dom_financiamiento').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_enganche']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d+\.\d+$/;
        $('#conf_dom_financiamiento').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_equipo']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d\.\-_\/\\\']){1,50}$/;
        $('#conf_dom_equipo').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

   

    //-----------------------------------------------------

    $("input[name='dom_imei']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^\d{15}$/;
        $('#conf_dom_imei').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    //-----------------------------------------------------

    $("input[name='dom_concepto']").on('keyup', function () {
        var selector = $(this);
        var valorselector = $(this).val();
        var regex = /^([a-zA-ZñÑáéíóúÁÉÍÓÚ\s\d$()\.\-_\/\\\']){1,300}$/;
        $('#conf_dom_concepto').html(valorselector);
        colorvalidacion(selector, valorselector, regex);
        resetcoloremptyfields(selector, valorselector);
    });

    


});



