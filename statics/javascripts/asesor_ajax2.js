
var buttonsforsubmitevents2;
$(document).ready(function(){

    //AJAX para ingresar y leer datos en el modal de entrga a domicilio

    
    
    $('#botonguardardomicilio').prop('disabled', true);
    //evento para habilitar los botones de submit al hacer keyup o change en caso del select
    $('input').on('keyup',function(){
        buttonsforsubmitevents2();
    });

    $('select').change(function(){
        buttonsforsubmitevents2();
    });

    buttonsforsubmitevents2 = function () {
        //para enviar y guardar
        var mandatory = $('.mandatory2').length;
        var mandatorywithhassuccess = $('.has-success').find('.mandatory2').length;
        var simultaneusselector = $('.mandatory2.has-success').length;
        var total_validated = mandatorywithhassuccess + simultaneusselector;

        var witherror = $('.has-error').length;

        if (mandatory == total_validated) {
            $('#botonguardardomicilio').prop('disabled', false);
        } else {
            $('#botonguardardomicilio').prop('disabled', true);
        }
    }

    //----------------------------------------------------------------------------------------------
    // eventos principales de los botones
    // llaman a la funcion para capturar los datos de los campos y establece un status
    

    $('#botonguardardomicilio').on('click', function () {
        getDataFromFormAndSubmit2('pendiente');
        
    });

    
    //----------------------------------------------------------------------------------------------
    
    

    function getDataFromFormAndSubmit2(status) {
        var rol = $('body').data('rol');
        //console.log(rol);
        var userLoggedIn = $('#seccionconfirmarcaptura').data('user');
        //pestaña datos del plan
        var cliente = $("input[name='dom_cliente']").val();
        var fechaentrega = $("input[name='dom_fechaentrega']").val();
        var horarioentrega = $("input[name='dom_horarioentrega']").val();
        var contacto = $("input[name='dom_contacto']").val();
        var numamigrar = $("input[name='dom_numamigrar']").val();
        var destino = $("input[name='dom_destino']").val();
        var direccion = $("input[name='dom_direccion']").val();
        var colonia = $("input[name='dom_colonia']").val();
        var referencias = $("input[name='dom_referencias']").val();
       

        //pestaña datos personales
        var folio = $("input[name='dom_folio']").val();
        var canal = $("#dom_canal").val();
        var plan = $("#dom_plan").val();
        var tipoactivacion = $("select[name='dom_tipoactivacion']").val();
        var ctaconsolidar = $("input[name='dom_ctaconsolidar']").val();
        var email = $("input[name='dom_email']").val();
        var financiamiento = $("input[name='dom_financiamiento']").val();
        var enganche = $("input[name='dom_enganche']").val();
        var equipo = $("input[name='dom_equipo']").val();
        var imei = $("input[name='dom_imei']").val();
        var concepto = $("input[name='dom_concepto']").val();
       
        
        

        //pestaña confirmar captura
        var observaciones = $("input[name='observacionesadicionales2']").val();
        
        var statusofrequest = status; 
        
        insertdata2(rol, userLoggedIn, cliente, fechaentrega, horarioentrega,
            contacto, numamigrar,destino,direccion,colonia,referencias,
            folio,canal,plan,tipoactivacion,ctaconsolidar,email,
            enganche,financiamiento,equipo,imei,concepto,observaciones,statusofrequest
            );

    };


    function insertdata2(
        rol, userLoggedIn, cliente, fechaentrega, horarioentrega,
        contacto, numamigrar, destino, direccion, colonia, referencias,
        folio, canal, plan, tipoactivacion, ctaconsolidar, email,
        enganche,financiamiento, equipo, imei, concepto, observaciones, statusofrequest
    ){
        //validacion de campos nulos
        if (cliente == undefined) {cliente = null;}
        if (fechaentrega == undefined) { fechaentrega = null; }
        if (horarioentrega == undefined) { horarioentrega = null; }
        if (contacto == undefined) { contacto = null; }
        if (numamigrar == undefined) { numamigrar = null; }
        if (destino == undefined) { destino = null; }
        if (direccion == undefined) { direccion = null; }
        if (colonia == undefined) { colonia = null; }
        if (referencias == undefined) { referencias = null; }
        
        if (folio == undefined) { folio = null; }
        if (canal == undefined) { canal = null; }
        if (plan == undefined) { plan = null; }
        if (tipoactivacion == undefined) { tipoactivacion = null; }
        if (ctaconsolidar == undefined) { ctaconsolidar = null; }
        if (email == undefined) { email = null; }
        if (enganche == undefined) { enganche = null; }
        if (financiamiento == undefined) { financiamiento = null; }
        if (equipo == undefined) { equipo = null; }
        if (imei == undefined) { imei = null; }
        if (concepto == undefined) { concepto = null; }

        if (observaciones == undefined) { observaciones = null; }
        
        
        

        //ajax
        if ((userLoggedIn != '') && (rol != 7)){

            $.post('/insertdatadomicilio', {
                userLoggedIn: userLoggedIn,
                
                //---------------------------
                cliente: cliente,
                fechaentrega: fechaentrega,
                horarioentrega: horarioentrega,
                contacto: contacto,
                numamigrar: numamigrar,
                destino: destino,
                direccion: direccion,
                colonia: colonia,
                referencias: referencias,
                
                // ---------------------------
                folio: folio,
                canal: canal,
                plan: plan,
                tipoactivacion: tipoactivacion,
                ctaconsolidar: ctaconsolidar,
                email: email,
                enganche: enganche,
                financiamiento: financiamiento,
                equipo: equipo,
                imei: imei,
                concepto: concepto,

                // -------------------------------
                observaciones: observaciones,
                //status
                statusofrequest: statusofrequest
            }, function(data) {

                    //solo para que el idshipping no sea tomado como undefined le asigno un valor
                    idshipping = 0;
                    if (data == 'Correcto') {
                        alert("Captura enviada correctamente");
                        filltable();
                        socket.emit('setWebsocketUpdate', { route: 'actualizardomiciliofromasesor'});
                        resetfields();
                        resetfields2();
                        $('#capturadomiciliomodal').modal('hide');
                    } else {
                        alert("Error al guardar los datos")
                    }
                
                
            }
            )}else{
            alert('Debido a ser perfil de capacitación no es posible la inserción a la base de datos');
        }
    }


    //-------------------------------------------------------

    //ajax para cargar datos de captura previa en la de entrega a domicilio par autocompletar datos al clickear el boton $('#btn_domsearchdata').

    $('#btn_domsearchdata').click(function(){
        var folio = $('#dom_folio').val();
        //var canal = $('#dom_canal').val();
        
        //hacer la llamada a backend
        $.post('/getdatabysisact',{folio: folio},function(response){

            if(response.length == 0){
                
                $('#dom_plan').val('').trigger('change');
                $('#dom_tipoactivacion').val('default').trigger('keyup');
                $('#dom_ctaconsolidar').val('').trigger('keyup');
                $('#dom_email').val('').trigger('keyup');
                $('#dom_equipo').val('').trigger('keyup');
                $('#dom_cliente').val('').trigger('keyup');
            }else{

                if (response[0]["cliente"] == undefined) {
                    $('#dom_cliente').val("");
                } else {
                    $('#dom_cliente').val(response[0]["cliente"]).trigger('keyup');
                    //capturo el cliente en caso de existir
                    var cliente = $('#dom_cliente').val();
                    $('#conf_dom_cliente').html(cliente);
                }

                if (response[0]["idplan"] == undefined) {
                    $('#dom_plan').val("");
                } else {
                    $('#dom_plan').val(response[0]["idplan"]).trigger('change');
                }

                if (response[0]["tipo"] == undefined || response[0]["tipo"] == "") {
                    //$('#dom_tipoactivacion').val("");
                } else {
                    var tipo = response[0]["tipo"];
                    var regex_tipo = /\d/;
                    if (tipo == "lineanueva"){
                        $('#dom_tipoactivacion').val('lineanueva').trigger('change');
                        $('#conf_dom_tipoactivacion').html("Linea nueva");
                    }else if(tipo == "msr"){
                        $('#dom_tipoactivacion').val('msr').trigger('change');
                        $('#conf_dom_tipoactivacion').html("MSR");
                    }else if(tipo.match(regex_tipo)){
                        $('#dom_tipoactivacion').val('migracion').trigger('change');
                        $('#conf_dom_tipoactivacion').html("Migración");
                    }else{
                        console.log("Error al validar tipo de activacion o esta vacio");
                        console.log(tipo);
                        $('#dom_tipoactivacion').val('default').trigger('change');
                        $('#conf_dom_tipoactivacion').html("");
                    }
                    
                }

                if (response[0]["cuenta"] == undefined) {
                    $('#dom_ctaconsolidar').val("");
                } else {
                    $('#dom_ctaconsolidar').val(response[0]["cuenta"]).trigger('keyup');
                }

                if (response[0]["email"] == undefined) {
                    $('#dom_email').val("");
                } else {
                    $('#dom_email').val(response[0]["email"]).trigger('keyup');
                }

                if (response[0]["equipo"] == undefined) {
                    $('#dom_equipo').val("");
                } else {
                    $('#dom_equipo').val(response[0]["equipo"]).trigger('keyup');
                }
                
                
                
                
                
                
            }
            
           
        });
    });

    //----------------------------------------------------
    //AJAX para insertar datos de modal de entrega a domicilio

    
    
});