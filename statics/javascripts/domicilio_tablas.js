var maintable, fillcounters;
$(document).ready(function(){

    

    //LLENA LAS TABLAS DEL PERFIL DEL ANALISTA A DOMICILIO
    maintable = function (status) {  
        $.post('/getDataForTableAnalista',{status:status},function (response) {
            var table_body = [];

            

            for (i = 0; i < response.length; i++) {
                table_body += "<tr id='row" + response[i]["idshipping"] +"' class='rowhover' data-idshipping='" + response[i]["idshipping"]+"'>";

                table_body += '<td>';
                table_body += response[i]["created"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["asesor"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["cliente"];
                table_body += '</td>';

                table_body += '<td>';
                table_body += response[i]["contacto"];
                table_body += '</td>';

                if (response[i]["guidenumber"] == null) {
                    table_body += '<td>';
                    table_body += "N/A"
                    table_body += '</td>';
                } else {
                    table_body += '<td>';
                    table_body += response[i]["guidenumber"];
                    table_body += '</td>';
                }


                table_body += '<td>';
                table_body += response[i]["status"];
                table_body += '</td>';

                if (response[i]["comments"] == null){
                    table_body += '<td>';
                    table_body += "N/A"
                    table_body += '</td>'; 
                }else{
                    table_body += '<td>';
                    table_body += response[i]["comments"];
                    table_body += '</td>';
                }
                
                table_body += '</tr>';
            }
            $('#tbody_domicilio').html(table_body);
        });
    }
    //al cargar por primera vez se carga por default del 
    //de pendientes
    maintable("pendiente");



    //LLENA LOS CONTADORES DEL PERFIL DE ANALISTA DOMICILIO
    //DE ACUERDO AL ESTATUS ACTUAL
    fillcounters = function(){
        
        $.get('/getDataLabelsCount',function(response){
            
            var contador_totales = 0;
            //reseteo todos los botones a cero
            //antes de escribir los nuevos valores
            
            $('.numberdata').html(0);

           
                    
                $('#cambioasinergia').html(response[0]['Cambio a sinergia']);
                        
                    
                $('#canceladaerrmensajeria').html(response[0]['Cancelada/Err.Mensajeria']);
                        
                    
                $('#equiponodisponible').html(response[0]['Equipo no disponible']);
                        
                    
                $('#guiaentregada').html(response[0]['Guia entregada']);
                        
                    
                $('#pendiente').html(response[0]['pendiente']);
                        
                    
                $('#canceladaporcliente').html(response[0]['Cancelada por cliente']);
                        
                    
                $('#clientenocontesta').html(response[0]['Cliente no contesta']);
                        
                    
                $('#errasesornombreerroneo').html(response[0]['Err.Asesor/Nombre erroneo']);
                        
                    
                $('#guiaenviada').html(response[0]['Guia enviada']);
                        
                    
                $('#tramiteincompleto').html(response[0]['Trámite incompleto']);
                        
                    
                $('#canceladaerrasesor').html(response[0]['Cancelada/Err.Asesor']);
                        
                    
                $('#confirmadopendenvio').html(response[0]['Confirmado/Pend.Envío']);
                        
                    
                $('#errasesornotieneid').html(response[0]['Err.Asesor/No tiene ID']);
                        
                    
                $('#nocuentaconpago').html(response[0]['No cuenta con pago']);
                        
                   
               response.forEach(element => {
                   for (key in element) {
                       if (element.hasOwnProperty(key)) {
                           if (element[key]) {
                               contador_totales += element[key];
                           }
                       }
                   }
               });
                        
            $('#todos').html(contador_totales);
            
        
        });

        

       
    }

    fillcounters();

});