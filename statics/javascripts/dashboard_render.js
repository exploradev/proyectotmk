//CONTROL DE LINKS A MODO DE APARTADOS CON LAS DIFERENTES SECTIONS

$(document).ready(function () {
    $('#linkfooterdashboard').click(function(){
        $('#bigbodyconnections').css('display', 'none');
        $('#bigbodycorrections').css('display', 'none');
        $('#bigbody').css('display','flex');
        $('#bigbodyrefered').hide()
    });

    $('#linkfooterconexiones').click(function () {
        $('#bigbody').css('display', 'none');
        $('#bigbodycorrections').css('display', 'none');
        $('#bigbodyconnections').css('display', 'flex');
        $('#bigbodyrefered').hide()
    });

    $('#linkfootercorrecciones').click(function () {
        $('#bigbody').css('display', 'none');
        $('#bigbodyconnections').css('display', 'none');
        $('#bigbodycorrections').css('display', 'flex');
        $('#bigbodyrefered').hide()
    });

    $('#linkfooterreferidos').click(function () {
        $('#bigbody').hide();
        $('#bigbodycorrections').hide()
        $('#bigbodyrefered').show()
    });
});